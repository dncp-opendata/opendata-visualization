/** Gráfico de contratos por entidad, convocante, categoría y tipo de procedimiento **/
var anioActual = "2014",
	nivelActual = "poder-ejecutivo",
	popactual = null;
var es_ES = {
	    "decimal": ".",
	        "thousands": "\xa0",
	        "grouping": [3],
	        "currency": ["", " руб."],
	        "dateTime": "%A, %e %B %Y г. %X",
	        "date": "%d.%m.%Y",
	        "time": "%H:%M:%S",
	        "periods": ["AM", "PM"],
	        "days": ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
	        "shortDays": ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
	        "months": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	        "shortMonths": ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
	}
var ES = d3.locale(es_ES);

var width2 = 1000,
	height2 = 600,
	rectWidth = 20,
	rectHeight = 10,
	nodeStrokeW = 0.5,
	padding = 1,
	legendRectSize = 18;
	legendSpacing = 4;

var tooltip = d3.select("body")
    .append("div")
    .style("position", "absolute")
    .style("z-index", "10")
    .style("visibility", "hidden")
    .style("color", "white")
    .style("padding", "8px")
    .style("background-color", "rgba(0, 0, 0, 0.75)")
    .style("border-radius", "6px")
    .style("font", "12px sans-serif")
    .text("tooltip");

var	chart = null,
	force = null,
	dataChart2,
	tiposDeProcedimiento,
	categorias;

var objetosDelLlamado = ["Bienes","Consultorías","Locación inmuebles","Locación muebles","Obras","Servicios","Tierras"]
var objetosDelLlamadoMap = {"Bienes":"#542788", "Consultorías":"#998EC3", "Locación inmuebles":"#D8DAEB",
		"Locación muebles":"#FEE0B6", "Obras":"#F1A340", "Servicios":"#B35806","Tierras":"#ddd"};
var tipos = {"centralizado":["Poder Ejecutivo", "Poder Legislativo", "Poder Judicial", 
	         					"Contraloría General de la República", "Otros organismos del estado"],
			"descentralizado":["Banca Central del Estado","Gobiernos Departamentales",
								"Entes Autónomos y Autárquicos","Entidades Públicas de Seguridad Social",
								"Empresas Públicas","Empresas Mixtas","Entidades Financieras Oficiales",
								"Universidades Nacionales"],
			"municipalidades":["Municipalidades"]}

/* Extensión fija de un año, porque las x del conjunto de datos se genera en base a la fecha */
var xScale = d3.scale.linear()
	.domain([0,366])
	.range([0, width2])
	
var xScaleTime = d3.time.scale()
    .domain([new Date(2014, 0, 1), new Date(2014,11,31)])
    .range([0, width2]);

var initForce = function() {
	//console.log(dataChart2.length);
	var maxElem = d3.max(dataChart2, function(d) { return d.monto_adjudicado; })
	var area = d3.scale.sqrt().domain([0, maxElem]).range([0, 10]);
	
	/*var rScale = function(d) {
		if (d <= 100000000)
			return 3;
		else if (d <= 1000000000)
			return 6;
		else if (d <= 5000000000)
			return 9;
		else
			return 12;		
	}*/
	var montos = _.map(dataChart2, function(elem){return parseInt(elem.monto_adjudicado)});
	//var cantidades = _.chain(contratos).flatten().map(function(contrato){ return parseInt(contrato.cantidad)}).value();
	console.log(montos);
	var rScale = d3.scale.sqrt().domain([0, _.max(montos)]).range([3, 25]);
	
	for (var j = 0; j < dataChart2.length; j++) {
		var index = d3.round(Math.random() * 6,0);
		/* La x se genera en base a la fecha */
		var now = dataChart2[j].fecha_firma_contrato;
		var test = now.substring(0,10);
		var dt = new Date(test);
		var start = new Date(dt.getFullYear(), 0, 0);
		var diff = dt - start;
		var oneDay = 1000 * 60 * 60 * 24;
		var day = Math.floor(diff / oneDay);
		//var true_x = day;
		dataChart2[j].r = rScale(dataChart2[j].monto_adjudicado);
        //dataChart2[j].r = area(dataChart2[j].monto_adjudicado);
        //dataChart2[j].r = 1 + Math.random() * 5;
		dataChart2[j].x = day;
		//dataChart2[j].x = Math.random() * width2;
		dataChart2[j].y = Math.random() * height2;
		// obtener el mes dada la fecha
		dataChart2[j].dia = day; //seterle el dia segun la fecha
		dataChart2[j].mes = dt.getMonth(); //setearle el mes segun la fecha
		//console.log(dataChart2[j].mes);
		//dataChart2[j].true_x = true_x;
		//dataChart2[j].true_y = 250;
		dataChart2[j].objeto = objetosDelLlamado[index];
    }
	
	_.mixin({ 
		pluckMany : function() {
		    // get the property names to pluck
		    var source = arguments[0];
		    var propertiesToPluck = _.rest(arguments, 1);
		    return _.map(source, function(item) {
		        var obj = {};
		        _.each(propertiesToPluck, function(property) {
		            obj[property] = item[property];
		            if (property == 'dia')
		            	obj['name'] = item[property];
		        });
		        //obj['name'] = obj['dia'];
		        obj['value'] = 1;
		        return obj;
		    });
		}
	});
	
	/* Funcion para ubicar los centros - Variable de agrupacion: mes */
	var getCenters = function (vname, size) {
		  var centers, map;
		  var circulitos;
		  
          //circulitos = _.countBy(_.pluck(data, vname),function(d) { return d } );

         /* _.each(dataChart2, function(d){ 
              if(d[vname])
                circulitos[d[vname]] = parseInt(d.dia);
          });*/
		  console.log(dataChart2);
		  /*centers = _.uniq(_.pluck(dataChart2, vname)).map(function (d) { //A
		    return {name: d, value: 1, dia:d};
		  });*/

		  centers = _.chain(dataChart2).pluckMany( vname, "categoria_codigo").value();
		  
		  map = d3.layout.treemap().size(size).ratio(1/1).sort(function(a,b){return b.dia - a.dia}).mode("slice"); //B
		  var i = 0;
		  //console.log(centers);
		  map.nodes({children: centers}); //C
		  /*while (i < centers.length) {
			  var a = centers[i];
			  if (a.categoria == '17')
				  centers[i].y = centers[i].y + 150;
			  if (a.categoria == '18')
				  centers[i].y = centers[i].y + 300;
			  if (a.categoria == '22')
				  centers[i].y = centers[i].y + 450;
			  if (a.categoria == '24')
				  centers[i].y = centers[i].y + 600;
			  i+=1;
		  }*/
		  //console.log(centers);

		  return centers; //D
		};
		

	chart = d3.select("#chart2")
		.append("svg")
			.attr("width", width2)
		  	.attr("height", height2)
		  	.attr("pointer-events", "all")
	  	.append("g")
	    	.attr("transform", "translate(50,10)")
	  	.append('g');

	chart.append('rect')
	   	.attr('width', width2)
	   	.attr('height', height2)
	   	.attr('fill', 'white');

	var axis = d3.svg.axis().scale(xScaleTime).orient("bottom")
		//.ticks(d3.time.months.range(new Date(2014, 0, 1),new Date(2014, 11, 31),3))
		//.tickSize(5, 0)
		//.tickFormat(ES.timeFormat("%B"));

	var xAxis = chart.append("g").attr("class", "x axis")
	// .style({ 'stroke': '#000', 'fill': 'none', 'stroke-width': '0.2'})
	.call(axis)

	xAxis.selectAll('text').style({
		'stroke-width' : '0.1',
		'fill' : '#000'
	});

	/* Línea de puntos en medio del gráfico */
	chart.append("line")
        .attr("class", 'd3-dp-line')
        .attr("x1", 50)
        .attr("y1", 250)
        .attr("x2", 950)
        .attr("y2", 250)
        .style("stroke-dasharray", ("1, 1"))
        .style("stroke-opacity", 0.9)
        .style("stroke-width", 0.3)
        .style("stroke", "#000");

	var cValue = function(d) { return d.objeto;};

    /* Leyenda de colores */
	var hor = -150;
	var ver = 550;
	var legend = chart.selectAll('.legend')
		.data(objetosDelLlamado)
	  	.enter()
	  	.append('g')
	  	.attr('class', 'legend')
	  	.attr('transform', function(d, i) {
	    var height = legendRectSize + legendSpacing;
	    /*var offset =  height * objetosDelLlamado.length / 2;
	    var horz = -2 * legendRectSize + 930;
	    var vert = i * height - offset + 350;*/
	    var horz, vert;
		hor = hor + 150;
	    return 'translate(' + hor + ',' + ver + ')';
	});

	/* Leyenda de tamaños de circulos */
	legend.append('rect')
	  	.attr('width', legendRectSize)
	  	.attr('height', legendRectSize - 10)
	  	.style('fill', alternatingColorScale)
	  	.style('stroke', alternatingColorScale);

	legend.append('text')
	  	.attr('x', legendRectSize + legendSpacing)
	  	.attr('y', legendRectSize - legendSpacing - 6)
  		.text(function(d) { return d; });

	/*setInterval(function(){
		  nodes.push({id: ~~(Math.random() * 10)});
		  force.start();
	});*/
	
   	var nodes = chart.selectAll("circle")
   		.data(dataChart2)
	
	nodes.enter().append("circle")
		.attr("class", "node")
	    .attr("cx", function(d) { return xScale(d.x) })
	    .attr("cy", function(d) { return d.y })
	    .attr("r", function(d) { return d.r })
			.style("fill", function(d) { 
				return alternatingColorScale(d.objeto);})
	  	.style("stroke", "black")
			.style("stroke-width", nodeStrokeW)
      	//.on("mouseover", function (d) { showPopover.call(this, d); })
		//.on("mouseout",	function (d) { removePopovers(); })
		.on("mouseover", mouseover)
		.on("mouseout", mouseout)
		.on("mousemove", mousemove)
		  
		
	/* Funcion para agrupar las burbujas hacia su centro */	
	function tick (centers, varname) {
		  var foci = {}; // Making an object here for quick look-up
		  for (var i = 0; i < centers.length; i++) {
		    foci[centers[i].name] = centers[i];
		  }
		  return function (e) { //A
		    for (var i = 0; i < dataChart2.length; i++) {
		      var o = dataChart2[i];
		      var f = foci[o[varname]];
		      o.y += ((f.y + (f.dy / 2)) - o.y) * e.alpha;
		      o.x += ((f.x + (f.dx / 2)) - o.x) * e.alpha;
		    }
		    nodes.each(collide(.11)) //B
		      .attr("cx", function (d) { return d.x; })
		      .attr("cy", function (d) { return d.y; });
		  }
		}
	
   	force = d3.layout.force()
	    //.gravity(0)
	    //.charge(0)
	    .friction(0)
   		//.alpha(0.8);
	   	.nodes(dataChart2)
	    .size([width2, height2]);
   	
	/* que se grafique en el tree layout con sus centros */
	var centers = getCenters("dia", [width2 - 50, height2 - 100]);
	force.on("tick", tick(centers, "dia"));
	//labels(centers)
    force.start();
    
	//force.on("tick", stepForce);
}

function mouseover(d) {
	//var parseDate = d3.time.format("%Y-%m-%d");
	var commaFormat = d3.format(',');
	tooltip.html("<strong>"+ d.dia +"</strong><br/>"+"<strong>Convocante:</strong> " + d.convocante + "<br>" + 
			"<strong>Llamado:</strong> " + d.nombre_licitacion + "<br>" +
			"<strong>Proveedor:</strong> " + d.razon_social + "<br>" +
			"<strong>Fecha firma contrato:</strong> " + d.fecha_firma_contrato + 
			"<br><strong>Monto total adjudicado:</strong> " + commaFormat(d3.round(d.monto_adjudicado,0)) + " Gs.")
			.style("visibility", "visible");
	//d3.select(this).style("fill-opacity", function(d) { return 0.8; });
	d3.select(this).style("stroke-width", "2px");
}

function removePopovers () {
    $('.popover').each(function() {
    	d3.select(this).style("stroke-width", "0.5px");
      $(this).remove();
      popactual = null;
    });
  }

function showPopover (d) {
    //console.log("entro");
    $(this).popover({
      placement: 'auto right',
      container: 'body',
      trigger: 'manual',
      html : true,
      //title: 'Detalles del contrato',
      content: function() {
    	  /*var crudo = '<div class="tooltipo padding10" role="tooltip"> <table> <tbody> <tr> <td> <strong>' + d.convocante +
    	  ' </strong> </td> </tr> </tbody> </table> <table class="tab_sec mb10">'+
          ' <tbody> <tr> <td width="40%"> Fecha de contrato </td> <td style="text-align="right";"><strong>' + d.fecha_firma_contrato + '</strong></td> '+
          ' </tr> <tr> <td> Proveedor </td> <td style="text-align="right";"><strong>' + d.razon_social + '</strong> </td>'+
          ' </tr> <tr> <td> Código de contratación </td> <td style="text-align="right";"><strong>' + d.codigo_contratacion + '</strong> </td>'+
          ' </tr> <tr> <td> Monto adjudicado </td> <td style="text-align="right";"><strong>Gs. ' + parseInt(d.monto_adjudicado).toLocaleString() +
          ' </strong> </td> <td> </td> </tr> </tbody> </table> </div>';*/
    	  
    	  var crudo = '<div class="tooltipo padding10" role="tooltip">'+ '<strong>'+ d.dia +
    	  '</strong><br/>'+' <table> <tbody> <tr> <td> <strong>' + d.nombre_licitacion +
    	  '</strong><br/>'+' <table> <tbody> <tr> <td> <strong>' + d.razon_social +
    	  ' </strong> </td> </tr> </tbody> </table> <table class="tab_sec mb10">'+
          ' <tbody> <tr> <td width="40%"> Fecha de contrato </td> <td><strong>' + d.fecha_firma_contrato + '</strong></td> '+
          ' </tr> <tr> <td> Código de contratación </td> <td style="text-align="right";"><strong>' + d.codigo_contratacion + '</strong> </td>'+
          ' </tr> <tr> <td> Monto adjudicado </td> <td style="text-align="right";"><strong>Gs. ' + parseInt(d.monto_adjudicado).toLocaleString() +
          ' </strong> </td> <td> </td> </tr> </tbody> </table> </div>';
    	  return crudo;
      }
    });
    if(popactual !== d){
        popactual = d;
        $(this).popover('show');
        d3.select(this).style("stroke-width", "2px");
      }
}

function mousemove(d) {
	tooltip.style("top",
		    (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");
}

function mouseout(d) {
	tooltip.style("visibility", "hidden");
	//d3.select(this).style("fill-opacity", function(d) { return 1; });
	d3.select(this).style("stroke-width", "0.5px");

}

function randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

var stepForce = function () {
	var q, node //= d3.geom.quadtree(data),
      	i = 0,
      	n = dataChart2.length;

	var q = d3.geom.quadtree(dataChart2);

  	while (++i < n) {
    	node = dataChart2[i];
    	q.visit(collide(node));
    	xerr = node.x - node.true_x;
    	yerr = node.y - node.true_y;
    	node.x = node.x - xerr*0.8;
    	node.y = node.y - yerr*0.005;
  	}

	chart.selectAll("circle")
    	.attr("cx", function(d) { return xScale(d.x) })
      	.attr("cy", function(d) { return d.y });

	//force.start();
}

var alternatingColorScale = function(d)
{
	return objetosDelLlamadoMap[d];
}

//var norm = d3.random.normal(0, 4.0)
/*function norm() {
  var res, i;
  res = 0;
  for (i = 0; i < 10; i += 1) {
    res += Math.random()*2-1
  }
  return res;
}*/

var padding = 5;
var maxRadius = d3.max(_.pluck(dataChart2, 'r'));

function collide(alpha) {
    var quadtree = d3.geom.quadtree(dataChart2);
    return function (d) {
      var r = d.r + maxRadius + padding,
          nx1 = d.x - r,
          nx2 = d.x + r,
          ny1 = d.y - r,
          ny2 = d.y + r;
      quadtree.visit(function(quad, x1, y1, x2, y2) {
        if (quad.point && (quad.point !== d) ) {
          var x = d.x - quad.point.x,
              y = d.y - quad.point.y,
              l = Math.sqrt(x * x + y * y),
              r = d.r + quad.point.r + padding;

            if (l < r) {
              l = (l - r) / l * alpha;
              d.x -= x *= l;
              d.y -= y *= l;
              quad.point.x += x;
              quad.point.y += y;
            }
        }

        return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
      });
    };
  }

/*function collide(node) {
	  var r = node.r,
	    nx1,
	    nx2,
	    ny1,
	    ny2,
	    xerr,
	    yerr;
	    
	  nx1 = xScale(node.x) - r;
	  nx2 = xScale(node.x) + r;
	  ny1 = node.y - r;
	  ny2 = node.y + r;
	      
	  return function(quad, x1, y1, x2, y2) {
	    if (quad.point && (quad.point !== node)) {
	      var x = xScale(node.x) - xScale(quad.point.x),
	          y = node.y - quad.point.y,
	          l = Math.sqrt(x * x + y * y),
	          r = node.r + quad.point.r;
	      if (l < r) {
	        // we're colliding.
	        var xnudge, ynudge, nudge_factor;
	        nudge_factor = (l - r) / l * .4;
	        xnudge = x*nudge_factor;
	        ynudge = y*nudge_factor;
	        node.x -= xnudge;
	        node.y -= ynudge;
	        quad.point.x += xnudge;
	        quad.point.y += ynudge;
	      }
	    }
	    return xScale(x1) > nx2
	        || xScale(x2) < nx1
	        || y1 > ny2
	        || y2 < ny1;
	  };
	}*/

function obtenerContratosPorEntidad(anio,nivel,entidad,agrupacion) {
	//console.log(anio);
	//console.log(nivel);
	//console.log(entidad);
	$.ajax({
        url: "http://localhost:9000/datos/interno/contratosPorAnioEntidad/" + anio + "/" + entidad,
        type:"get",
        success: function(response) {
           // console.log(response);
            if (!response){
                alert("Debe indicar al menos un filtro.");
                return null;
            } else {
            	dataChart2 = response["contratos"];
                initForce();
            	
            	
            }
        },
        error: function(xhr) {
            console.log('errror');
            return null;
	        }
    });
}

var agrupacionActual;

$('#buttonOptions').on('click', function(event) {
	var agrupacion = $(event.target).attr('id');
    if (force) {
        force.stop();
    }
    console.log("click en " + agrupacion);
	//drawChart2(entidadActual,agrupacion);
});

function drawChart2(entidad,agrupacion) {
	//$("#chart2").empty();
	//console.log("agrupacion " + agrupacion);
	agrupacionActual = agrupacion;
	if (agrupacion == "contratos")
		obtenerContratosPorEntidad(anioActual,nivelActual,entidad);
	/*else if (agrupacion == "categorias") {
		dataService.getCategorias(cargarCategorias);
	}
	else if (agrupacion == "tiposDeProcedimiento"){
		dataService.getTiposDeProcedimiento(cargarTipos);
	}
	else {
		dataService.getConvocantes(cargarConvocantes);
	}*/
}

function drawChartAgrupacion(agrupacion) {
	console.log("Vista de contratos por agrupacion: " + agrupacion);
}


drawChart2("MINISTERIO DE SALUD PÚBLICA Y BIENESTAR SOCIAL","contratos");