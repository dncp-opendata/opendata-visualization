$('#tabla-descarga tbody').on('click', 'td.descarga-conjuto-datos', function () {
    var link = this.firstElementChild.getAttribute('href');
    var path = link.split('/');
    var id = path[path.length-1];
    var modulo = path[path.length-2];
    ga('send', 'event', 'OpenData', modulo, modulo + ' '+ id);
});