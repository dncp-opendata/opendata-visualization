FROM nginx:stable
COPY deploy.sh /deploy.sh
COPY ./calendario_llamados/ /var/www/calendario_llamados/
COPY ./ciclo_licitacion/ /var/www/ciclo_licitacion/
COPY ./contratos_monto/ /var/www/contratos_monto/
COPY ./datatable/ /var/www/datatable/
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
RUN echo "daemon off;" >> /etc/nginx/nginx.conf