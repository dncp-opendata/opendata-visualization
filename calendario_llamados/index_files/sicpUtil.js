/**
 * Utilidades para el proyecto Sicp.
 * 
 * Dependencias:
 * 	1. bigdecimal/mathcontext.js
 *  2. bigdecimal/bigdecimal.js
 */

debug = false;

function fireEvent(idElemento,event){
	var element = null;
	element = document.getElementById(idElemento);	
		
	if(element != null) {
	    if (document.createEventObject){
	        // dispatch for IE
	        var evt = document.createEventObject();
	        return element.fireEvent('on'+event,evt)
	    }
	    else{
	        // dispatch for firefox + others
	        var evt = document.createEvent("HTMLEvents");
	        evt.initEvent(event, true, true ); // event type,bubbling,cancelable
	        return !element.dispatchEvent(evt);
	    }
	}
}

montoConverter_debug = false;

/**
 * Convierte los montos expresados en castellano #.##0,00 a formato ingles. 
 * Retorna un objeto de tipo BigDecimal
 * Dependiente de las libreras de bigDecimal.
 * @param monto
 * @return
 */
function montoConverter(monto)
{
	monto = (monto + "").replace(/\./g, "").replace(/\,/g, ".");
	
	if(isNaN(monto))
		return new BigDecimal("0");
		
	return new BigDecimal(monto);
}

/**
 * Se utiliza para para deshabilitar un input segn su ID 
 */
function disableElement(btn){
	var elem = document.getElementById(btn);
    if(elem!= null)
    	elem.disabled = true;
}
/**
* Se utiliza para para habilitar un input segn su ID 
*/
function enableElement(btn){
	var elem = document.getElementById(btn);
    if(elem != null)
    	elem.disabled = false;
}

window.onresize = function()
{
	calculateBottomDocument();
};

document.onload = function(){
	calculateBottomDocument();
};

window.onload = function(){
	calculateBottomDocument();
};

/*document.observe("dom:loaded", function() {
	calculateBottomDocument();
});*/


function calculateBottomDocument(ie_bug)
{
	var btmMsgDiv = document.getElementById('bottomFixedMessage'); 
	if(btmMsgDiv != null)
	{
		height = parseInt(getInnerHeight(), 10);
		width = parseInt(getInnerWidth(), 10);
		
		if (window.innerHeight)
		{
			aux = 1007;
			ie_bug = 0;
		}
		else
			aux = 991;
		
		if(width < aux)
			pos = height - 40;
		else
			pos = height - 25;
		
		//alert(getInnerWidth());
		btmMsgDiv.style.top = pos + "px";
	}
};

function getInnerHeight()
{
	if (window.innerHeight)
	{
		//navegadores basados en mozilla
		return window.innerHeight;
	}
	else
	{
		//Navegadores basados en IExplorer, es que no tengo innerheight
		return document.documentElement.clientHeight;
	};
};

function getInnerWidth()
{
	if (window.innerHeight)
	{
		//navegadores basados en mozilla
		return window.innerWidth;
	}
	else
	{
		//Navegadores basados en IExplorer, es que no tengo innerheight
		return document.documentElement.clientWidth;
	};
};

function validateTextAreaLength(value, maxlength){
	if(value.length > maxlength){ 
		alert('Ha superado el maximo de caracteres permitidos para este campo.');
		value = value.substring(0,maxlength-1); 
	}
	return value;

};


/**
 * Los IDs de los campos de filtro para los selectores modales estn en duro en los templates
 * 
 * @return
 **/
function effectGrowMensajes() 
{
	var inputs = $$('dl[id*=mensajesArriba]');
		
	inputs.each(function(node)
	{
		if(node.innerHTML != null || node.innerHTML != '')
			Effect.Grow(node.id);		
	});
	
	var inputsAbajo = $$('dl[id*=mensajesAbajo]');
	
	inputsAbajo.each(function(node2)
	{
		if(node.innerHTML != null || node.innerHTML != '')
			Effect.Grow(node2.id);		
	});
};