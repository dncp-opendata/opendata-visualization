//var urlBase = "http://localhost:9001/datos/data/";
var loc = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var urlBase = loc + "/datos/data/";

var planificacionColumns = [ 
        "Año", "Id", "Id llamado", "Nombre licitación",
		"Convocante", "Categoría", "Tipo procedimiento", "Fecha estimada",
		"Estado de la planificación", "Objeto licitación" ], 
convocatoriaColumns = [
		"Año", "Id", "Id llamado", "Nombre licitación", "Convocante",
		"Categoría", "Tipo procedimiento", "Estado de la convocatoria",
		"Fecha de apertura de ofertas", "Sistema de adjudicación" ], 
adjudicacionColumns = [
		"Año", "Id", "Id llamado", "Nombre licitación", "Convocante",
		"Categoría", "Tipo procedimiento", "Estado de la adjudicación",
		"Monto adjudicado", "Fecha de publicación" ], 
contratoColumns = [ "Año", "Id",
		"Id llamado", "Nombre licitación", "Convocante", "Código contratación", 
		"Razon social", "Ruc", "Fecha firma contrato", "Monto total", "Vigencia contrato",
		"Moneda"], 
modificacionColumns = [
		"Año", "Id", "Id contrato", "Código contratación", "Tipo", "Monto",
		"Fecha modificación contrato", "Vigencia modificación de contrato" ];
catalogoColumns = ["Codigo" , "Nombre", "Nivel 1" ,"Nivel 2","Nivel 3","Nivel 4", "Codigo Niv.4"];


var columnData = {
	// para planificacion
	'Id' : 'id',
	'Id llamado' : 'idLlamado',
	'Nombre licitación' : 'nombreLicitacion',
	'Convocante' : 'convocante',
	'Categoría' : 'categoria',
	'Tipo procedimiento' : 'tipoProcedimiento',
	'Fecha estimada' : 'fechaEstimada',
	'Estado de la planificación' : 'estado',
	'Objeto licitación' : 'objetoLicitacion',
	'Año' : 'anioCsv',
	// para convocatoria
	'Estado de la convocatoria' : 'estado',
	'Fecha de apertura de ofertas' : 'fechaAperturaOferta',
	'Sistema de adjudicación' : 'sistemaAdjudicacion',
	// para adjudicacion
	'Estado de la adjudicación' : 'estado',
	'Monto adjudicado' : 'montoTotalAdjudicado',
	'Fecha de publicación' : 'fechaPublicacion',
	// para contratos
	'Código contratación' : 'codigoContratacion',
	'Razon social' : 'razonSocial',
	'Ruc' : 'ruc',
	'Fecha firma contrato' : 'fechaFirmaContrato',
	'Monto total' : 'montoAdjudicado',
	'Vigencia contrato' : 'vigenciaContrato',
	'Moneda' : 'moneda',
	// para modificaciones
	'Id contrato' : 'contratoId',
	'Tipo' : 'tipo',
	'Monto' : 'monto',
	'Fecha modificación contrato' : 'fechaEmisionCc',
	'Vigencia modificación de contrato' : 'vigenciaContrato',
	//para catalogo
	'Codigo': 'codigo',
	'Nombre': 'nombre',
	'Nivel 1': 'n1Nombre',
	'Nivel 2': 'n2Nombre',
	'Nivel 3': 'n3Nombre',
    'Nivel 4': 'n4Nombre',
	'Codigo Niv.4': 'n4Codigo'
};

var columnRenderers = {
	'Fecha estimada' : function(data, type, full, meta) {
		var date = moment(data);
		if (type == "display" && date.isValid()) {
			return date.format('DD/MM/YYYY');
		}
		return data;
	},
	'Fecha de apertura de ofertas' : function(data, type, full, meta) {
		var date = moment(data);
		if (type == "display" && date.isValid()) {
			return date.format('DD/MM/YYYY');
		}
		return data;
	},
	'Fecha de publicación' : function(data, type, full, meta) {
		var date = moment(data);
		if (type == "display" && date.isValid()) {
			return date.format('DD/MM/YYYY');
		}
		return data;
	},
	'Fecha firma contrato' : function(data, type, full, meta) {
		var date = moment(data);
		if (type == "display" && date.isValid()) {
			return date.format('DD/MM/YYYY');
		}
		return data;
	},
	'Fecha modificación contrato' : function(data, type, full, meta) {
		var date = moment(data);
		if (type == "display" && date.isValid()) {
			return date.format('DD/MM/YYYY');
		}
		return data;
	},
	'Monto adjudicado' : function(data, type, full, meta) {
		return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
	},
	'Monto total' : function(data, type, full, meta) {
		return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
	},
	'Monto' : function(data, type, full, meta) {
		return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
	}
}

function init_config(modulo) {
	/* Por medio del modulo obtener las columnas correspondientes */
	var tableColumns;
	
	if (modulo == "planificaciones") {
		tableColumns = planificacionColumns;
	} else if (modulo == "convocatorias") {
		tableColumns = convocatoriaColumns;
	} else if (modulo == "adjudicaciones") {
		tableColumns = adjudicacionColumns;
	} else if (modulo == "contratos") {
		tableColumns = contratoColumns;
	} else if (modulo == "modificaciones") {
		tableColumns = modificacionColumns;
	} else if (modulo == "catalogo") {
		tableColumns = catalogoColumns;
	} else {
		tableColumns = [];
	}
	
	var columnCount = tableColumns.length;
	
	return columns = tableColumns.map(function(c, i) {
		return {
			"title" : c,
			"data" : columnData[c],
			"visible" : (i < columnCount),
			"defaultContent" : "",
			"render": columnRenderers[c]
		};
	});
}