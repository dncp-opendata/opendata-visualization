
/**
Servicios de acceso a datos para la visualización del calendario de llamados
**/
var dataService = new function () {
 var loc = "API_BASE_HOST_URL";
  var serviceBase = loc + '/datos/api/v3/doc/';
  var serviceAuth = serviceBase + 'oauth/';
  var requestToken = 'NDcxMjc5YzQtNjI1Yi00YjNiLWE4Y2QtYTBlN2VlOGRhZThlOjE0MzE1OTk5LTg2ZDQtNDYyZS1iMDRlLWM3Y2VjNzkyZWU2Zg==';
  /** Obtiene la lista de categorías posibles de un llamado, ejecutando
  el callback en caso de éxito. **/
  getCategorias = function (callback) {
    $.getJSON(serviceBase + 'parameters/procurementCategories?items_per_page=100',
      function (data) {
        callback(data.list);
      });
  };

  /** Obtiene la lista de instituciones convocantes posibles de un llamado, ejecutando
  el callback en caso de éxito. **/
  getInstituciones = function (callback) {
    $.getJSON(serviceBase + 'procuringEntities?items_per_page=1000',
      function (data) {
        callback(data.list);
      });
  };

  /** Obtiene la lista de modalidades posibles de un llamado, ejecutando
  el callback en caso de éxito. **/
  getModalidades = function (callback) {
    $.getJSON(serviceBase + 'parameters/procurementMethods?items_per_page=100',
      function (data) {
        callback(data.list);
      });
  };

  /** Obtiene el resumen de llamados por fecha de entrega de ofertas, considerando los filtros
  de la visualización, ejecutando el callback en caso de éxito. **/
  getCalendario = function (categorias, instituciones, modalidades, desde, hasta, callback) {
    var params = {};

    if (instituciones) {
      params.buyer = instituciones;
    }
    if (modalidades) {
      params.procurementMethodDetails = modalidades;
    }
    if (categorias) {
      params.mainProcurementCategoryDetails = categorias;
    }
    if (desde) {
      params.tenderPeriod_from = desde;
    }
    if (hasta) {
      params.tenderPeriod_until = hasta;
    }

    $.getJSON(serviceBase + 'visualizations/minimal/tenders/count', params).done(function (data) {
      callback(data);
    }).fail(function (jqxhr, textStatus, error) {
      callback([]);
    })
  };

  /** Obtiene la lista de llamados por fecha de entrega de ofertas, considerando los filtros
  de la visualización, ejecutando el callback en caso de éxito. **/
  getEventos = function (categorias, instituciones, modalidades, desde, hasta, callback) {
    var params = {};

    if (instituciones) {
      params.buyer = instituciones;
    }
    if (modalidades) {
      params.procurementMethodDetails = modalidades;
    }
    if (categorias) {
      params.mainProcurementCategoryDetails = categorias;
    }
    if (desde) {
      params.tenderPeriod_from = desde;
    }
    if (hasta) {
      params.tenderPeriod_until = hasta;
    }

    $.getJSON(serviceBase + 'visualizations/minimal/tenders', params).done(function (data) {
      console.log("DATO DE API V3", data);
      data['list'].forEach(function (d) {
        d.slug = d.tender.id;
        d.descripcion = d.tender.title;
        d.id = d.tender.id;
        d.fecha_entrega_oferta = d.tender.tenderPeriod.endDate;
      });
      callback(data['list']);
    })
        .fail(function (jqxhr, textStatus, error) {
      console.log("EXPLOTO TODO", textStatus, error);
    })
  };

  /** Retorna la URL de un llamado, de acuerdo a su id encriptado, en el portal de la DNCP. **/
  getLlamadoUrl = function (id) {
    var baseUrl = 'https://www.contrataciones.gov.py/licitaciones/convocatoria/';
    var llamadoParam = encodeURIComponent(id) + '.html';
    return baseUrl + llamadoParam;
  };

  getAccessToken = function (callback) {
    var currentToken = keyStorage.load('access_token');
    if (currentToken) {
      console.log("Token con localStorage");
      callback(currentToken);
    } else {
      $.ajax({
        dataType: "json",
        type: "POST",
        url: serviceAuth + 'token',
        data: {'request_token': requestToken},
        success: function (data) {
          console.log("Token con AJAX");
          keyStorage.save("access_token", data.access_token, 14);
          callback(data.access_token);
        }
      });
    }
  };

  getJsonLd = function (url, callback) {
    getAccessToken(function (accessToken) {
      $.ajax({
        dataType: "json",
        type: "GET",
        url: url,
        headers: { 'Authorization': accessToken },
        success: function (data) {
          callback(data);
        }
      });
    });
  };

  return {
    getCategorias: getCategorias,
    getInstituciones: getInstituciones,
    getModalidades: getModalidades,
    getCalendario: getCalendario,
    getEventos: getEventos,
    getLlamadoUrl: getLlamadoUrl,
    getAccessToken: getAccessToken,
    getJsonLd: getJsonLd
  };
}();

var keyStorage = new function () {
  save = function (key, jsonData, expirationMin) {
    var expirationMS = expirationMin * 60 * 1000;
    var record = { value: JSON.stringify(jsonData), timestamp: new Date().getTime() + expirationMS };
    try {
      localStorage.setItem(key, JSON.stringify(record));
    } catch (e) {
      alert("Por favor actualice su navegador");
    }
    return jsonData;
  };

  load = function (key) {
    try {
      var record = JSON.parse(localStorage.getItem(key));
    } catch (e) {
      alert("Por favor actualice su navegador");
    }
    if (!record) { return false; }
    return (new Date().getTime() < record.timestamp && JSON.parse(record.value));
  };

  return {
    save: save,
    load: load
  }
}();
