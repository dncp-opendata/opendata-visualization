/** Ayuda * */

$(document).ready(
		function() {
			// check_url();
			setup_intro();
			$(".nav.navbar-nav  li").removeClass("active");
			// var url = check_url();
			// console.log(url);
			var url = check_url()
			switch (url) {
			case "logout":
				id = "#tab-logout";
			case "def":
				id = "#tab-def";
				break;
			case "def":
			case "def/adjudicacion":
			case "def/categoria":
			case "def/contrato":
			case "def/documentos":
			case "def/estado":
			case "def/forma_de_pago":
			case "def/fuente_de_financiamiento":
			case "def/planificacion":
			case "def/convocatoria":
			case "def/grupo_de_items":
			case "def/item":
			case "def/moneda":
			case "def/proveedor":
			case "def/sistema_de_adjudicacion":
			case "def/tipo_de_procedimiento":
			case "def/tipo_garantia_de_ofertas":
			case "def/unidad_de_medida":
			case "def/convocatoria":
			case "def/planificacion":
			case "def/modificacion_de_contrato":
				id = "#tab-def";
				break;
			case "datos/login":
			case "datos/reset":
			case "datos/signup":
			case "aplicaciones/crear":
			case "aplicaciones/modificar":
			case "aplicaciones/consultar":
			case "datos/aplicaciones":
				id = "#tab-mis-aplicaciones";
				break;
			case "visualizaciones":
			case "visualizaciones/contratos":
			case "visualizaciones/etapas-licitacion":
				id = "#tab-viz";
				break;
			case "datos/data":
			case "data/planificaciones":
			case "data/convocatorias":
			case "data/adjudicaciones":
			case "data/contratos":
			case "data/modificaciones":
				id = "#tab-datasets";
				break;
			default:
				id = "#tab-inicio";
			}
			$(id).addClass("active");
		});

function check_url() {
	var url = document.location.toString();
	if (url.charAt(url.length - 1) == "#") {
		url = url.substring(0, url.length - 2);
	}
	var list = url.split("/");
	var hash = list.pop();
	if (!isNaN(hash)) {
		hash = list.pop();
	}
	if (hash != "datos" && hash != "def" && hash != "visualizaciones") {
		hash = list.pop() + "/" + hash;
	}
	//console.log(hash);
	return hash;
};

function setup_intro() {
	var stepsInicio = [
   			{
   				intro : "Bienvenido al Portal de Datos Abiertos de la DNCP.</br></br>Este tutorial te guiará paso a paso a través de las diferentes funcionalidades disponibles. \
         </br></br>Haz click en siguiente para comenzar."
   			},
   			{
   				element : '#tab-datasets',
   				intro : "En esta sección puedes ver los conjuntos de datos de la DNCP.",
   				position : "right"
   			},
   			{
   				element : '#tab-mis-aplicaciones',
   				intro : "En esta sección puedes registrar tus aplicaciones.",
   				position : "right"
   			},
   			{
   				element : '#tab-diccionario',
   				intro : "En esta sección puedes ver el diccionario de datos.",
   				position : 'right'
   			},
   			{
   				element : '#tab-viz',
   				intro : "En esta sección puedes ver la galería de visualizaciones.",
   				position : 'right'
   			},
   			{
   				element : '#buttonExplorar',
   				intro : "Haz click aquí para ir a la lista de los conjuntos de datos disponibles.",
   				position : "top"
   			},
   			{
   				element : '#buttonVisualizaciones',
   				intro : "Haz click aquí para ir a la galería de visualizaciones.",
   				position : "top"
   			},
   			{
   				element : '#buttonAplicaciones',
   				intro : "Haz click aquí para ir al registro de aplicaciones.",
   				position : "top"
   			}

   	];
	
	var stepsConjutoDeDatos = [
			{
				intro : "Bienvenido al Portal de Datos Abiertos de la DNCP.</br></br>Este tutorial te guiará paso a paso a través de las diferentes funcionalidades disponibles. \
      </br></br>Haz click en siguiente para comenzar."
			},
			{
				element : '.tabla',
				intro : "Aquí puedes ver los datos de los diferentes servicios web disponibles.",
				position : "top"
			},
			{
				element : '#help-actions',
				intro : "Puedes navegar por la documentación del servicio, el diccionario, descargas del dataset y ejemplos de la API",
				position : 'left'
			},

	];

	var stepsMisAplicaciones = [
			{
				intro : "Bienvenido al Portal de Datos Abiertos de la DNCP.</br></br>Este tutorial te guiará paso a paso a través de las diferentes funcionalidades disponibles. \
      </br></br>Haz click en siguiente para comenzar."
			},
			{
				element : '#help-aplicaciones',
				intro : "En esta sección, puedes ver la lista de aplicaciones que tienes registradas. Para consultar una aplicación haz click sobre su nombre.",
				position : "right"
			}, {
				element : '#help-crear',
				intro : "Haz click aquí para registrar una nueva aplicación.",
				position : 'right'
			} ];

	var stepsMisAplicacionesCrear = [
			{
				intro : "Bienvenido al Portal de Datos Abiertos de la DNCP.</br></br>Este tutorial te guiará paso a paso a través de las diferentes funcionalidades disponibles. \
	    </br></br>Haz click en siguiente para comenzar."
			}, {
				element : '#edit-name',
				intro : "Especifica el nombre que tendrá la aplicación.",
				position : "right"
			}, {
				element : '#edit-description',
				intro : "Una breve descripción.",
				position : 'right'
			}, {
				element : '#edit-url',
				intro : "Y la página de acceso público.",
				position : 'right'
			}, {
				element : '#help-guardar',
				intro : "Haz click aquí para guardar los cambios.",
				position : 'right'
			} ];

	var stepsMisAplicacionesModificar = [
			{
				intro : "Bienvenido al Portal de Datos Abiertos de la DNCP.</br></br>Este tutorial te guiará paso a paso a través de las diferentes funcionalidades disponibles. \
 	    </br></br>Haz click en siguiente para comenzar."
			}, {
				element : '#edit-name',
				intro : "Puedes modificar el nombre de la aplicación.",
				position : "right"
			}, {
				element : '#edit-description',
				intro : "La descripción.",
				position : 'right'
			}, {
				element : '#edit-url',
				intro : "Y la página de acceso público.",
				position : 'right'
			}, {
				element : '#help-modificar',
				intro : "Haz click aquí para guardar los cambios.",
				position : 'right'
			} ];

	var stepsMisAplicacionesConsultar = [
			{
				intro : "Bienvenido al Portal de Datos Abiertos de la DNCP.</br></br>Este tutorial te guiará paso a paso a través de las diferentes funcionalidades disponibles. \
 	    </br></br>Haz click en siguiente para comenzar."
			},
			{
				element : '#view-app-details',
				intro : "En esta sección puedes ver un resumen de los datos de la aplicación.",
				position : "top"
			},
			{
				element : '#help-modificar-app',
				intro : "Haz click aquí para modificar los datos básicos de la aplicación.",
				position : 'right'
			},
			{
				element : '#help-eliminar-app',
				intro : "Puedes eliminar la aplicación.",
				position : 'right'
			},
			{
				element : '#help-regenerar-keys',
				intro : "Haz click aquí para regenerar consumer key y secret de la aplicación.",
				position : 'right'
			} ];

	var stepsVisualizaciones = [
			{
				intro : "Bienvenido al Portal de Datos Abiertos de la DNCP.</br></br>Este tutorial te guiará paso a paso a través de las diferentes funcionalidades disponibles. \
   	    </br></br>Haz click en siguiente para comenzar."
			},
			{
				element : '#button1',
				intro : "Haz click aquí para ir al gráfico de contratos por cantidad y monto.",
				position : "right"
			},
			{
				element : '#button2',
				intro : "Haz click aquí para ir al gráfico de fases de una licitación.",
				position : 'right'
			}, {
				element : '#button3',
				intro : "Haz click aquí para ir al calendario.",
				position : 'right'
			} ];

	var stepsContratos = [
			{
				intro : "Bienvenido al Portal de Datos Abiertos de la DNCP.</br></br>Este tutorial te guiará paso a paso a través de las diferentes funcionalidades disponibles. \
  	    </br></br>Haz click en siguiente para comenzar."
			},
			{
				element : '#mini-tutorial-1',
				intro : "Aquí puedes ver como interactuar con el gráfico de contratos por cantidad.",
				position : "top"
			},
			{
				element : '#buttonYears',
				intro : "Haz click aquí para seleccionar el año.",
				position : 'bottom'
			},
			{
				element : '#buttonFilters',
				intro : "Haz click aquí para seleccionar el conjunto de entidades a visualizar.",
				position : 'bottom'
			},
			{
				element : '#mini-tutorial-2',
				intro : "Aquí puedes ver como interactuar con el gráfico de contratos por monto.",
				position : 'top'
			},
			{
				element : '#buttonOptions',
				intro : "Haz click aquí para seleccionar el tipo de clasificación a visualizar.",
				position : 'bottom'
			} ];

	var stepsEtapasLicitacion = [
			{
				intro : "Bienvenido al Portal de Datos Abiertos de la DNCP.</br></br>Este tutorial te guiará paso a paso a través de las diferentes funcionalidades disponibles. \
	    </br></br>Haz click en siguiente para comenzar."
			},
			{
				element : '#encabezado',
				intro : "Aquí puedes ver el título, convocante y etapa en que se encuentra la licitación.",
				position : "bottom"
			},
			{
				element : '#search-bar',
				intro : "Ingresa un Id del llamado y haz click en <b>Ver licitación</b> para ver las etapas de la licitación.",
				position : 'bottom'
			},
			{
				element : '#ref-table',
				intro : "Aquí puedes ver las referencias de etapas y estados de la licitación.",
				position : 'top'
			},
			{
				element : '#embedder-button',
				intro : "Haz click aquí para obtener el código para incluir la visualización en tu sitio.",
				position : 'left'
			} ];

	$('#start-tour').click(function() {
		// console.log("Click!");
		var steps;
		// console.log(check_url());
		switch (check_url()) {
		case "datos":
			steps = stepsInicio;
			break;
		case "datos/data":
			steps = stepsConjutoDeDatos;
			break;
		case "datos/aplicaciones":
			steps = stepsMisAplicaciones;
			break;
		case "aplicaciones/consultar":
			steps = stepsMisAplicacionesConsultar;
			break;
		case "aplicaciones/crear":
			steps = stepsMisAplicacionesCrear;
			break;
		case "aplicaciones/modificar":
			steps = stepsMisAplicacionesModificar;
			break;
		case "visualizaciones":
			steps = stepsVisualizaciones;
			break;
		case "visualizaciones/contratos":
			steps = stepsContratos;
			break;
		case "visualizaciones/etapas-licitacion":
			steps = stepsEtapasLicitacion;
			break;
		default:
			steps = false;
		}

		introJs().setOptions({
			doneLabel : 'Salir',
			nextLabel : 'Siguiente &rarr;',
			prevLabel : '&larr; Anterior',
			skipLabel : 'Salir',
			steps : steps
		}).start();
	});
}