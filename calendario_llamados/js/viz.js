
var spinner, completedCalls = 0;

function loadSelect(data, value, description, id) {
	var options = '';
	for (var x = 0; x < data.length; x++) {
		options += '<option value="' + data[x][value] + '">' + data[x][description] + '</option>';
	}
	$(id).html(options);
}

/** Carga el select field de categorías **/
function loadCategorias(data) {
	data.sort((a, b) => (a.nombre > b.nombre) ? 1 : -1);
	loadSelect(data, 'id', 'nombre', '#categoria');
}

/** Carga el select field de instituciones **/
function loadInstituciones(data) {
	data.sort((a, b) => (a.name > b.name) ? 1 : -1);
	data.forEach(function (d) {
		d['identifier_id'] = d.identifier.id;
	});
	loadSelect(data, 'identifier_id', 'name', '#institucion');
}

/** Carga el select field de modalidades **/
function loadModalidades(data) {
	data.sort((a, b) => (a.nombre > b.nombre) ? 1 : -1);
	loadSelect(data, 'id', 'nombre', '#modalidad');
}

/**
Obtiene los datos para cargar el heat calendar chart.
agenda: es el chart de agenda que se despliega como parte de la visualización.
**/
function loadChart(agenda) {
	var instituciones = $('#institucion').select2('val').toString();
	var categorias = $('#categoria').select2('val').toString();
	var modalidades = $('#modalidad').select2('val').toString();
	var desde = parseDate($('#desde').val()) || new Date(2019, 0, 1);
	desde = desde.toJSON().substring(0, 10);
	var hasta = parseDate($('#hasta').val());
	if (hasta) {
		hasta = hasta.toJSON().substring(0, 10);
	}
	var result = {};

	dataService.getCalendario(categorias, instituciones, modalidades, desde, hasta, function (data) {
		var calendar = data.forEach(function (e) {
			var key = Math.round(moment(e.tender.tenderPeriod.endDate).toDate().getTime() / 1000).toString();
			result[key] = e.total;
		});
		drawChart(result, agenda);
	});
}

/**Parsea las fechas en el formato del input de fechas de la visualización**/
function parseDate(input) {
	if (input) {
		var fecha = input.split('-');
		var anho = parseInt(fecha[2]);
		var mes = parseInt(fecha[1]) - 1;
		var dia = parseInt(fecha[0]);
		return new Date(anho, mes, dia);
	}
}

/**
Dibuja el heat calendar en la parte superior de la visualización.
rows: cantidad de llamados por fecha
agenda: es el chart de agenda que se despliega como parte de la visualización.
**/
function drawChart(rows, agenda) {
	$('#cal-heatmap').html('');
	var cal = new CalHeatMap();
	cal.init({
		data: rows,
		cellSize: 21,
		range: 7,
		legend: [5, 15, 25, 35],
		domain: "month",
		subDomain: "day",
		nextSelector: "#next-month",
		previousSelector: "#previous-month",
		itemName: ["llamado", "llamados"],
		subDomainTextFormat: "%d",
		domainLabelFormat: function (date) {
			mes = moment(date).format("MMMM");
			return mes.charAt(0).toUpperCase() + mes.substring(1);
		},
		subDomainTitleFormat: {
			empty: "{date}",
			filled: "{count} {name} el {date}"
		},
		subDomainDateFormat: function (date) {
			return moment(date).format("LL");
		},
		onClick: function (date, nb) {
			agenda.fullCalendar('changeView', 'agendaDay');
			agenda.fullCalendar('gotoDate', date);
		},
		legendTitleFormat: {
			lower: "menos de {min} {name}",
			inner: "entre {down} y {up} {name}",
			upper: "más de {max} {name}"
		},
		legendOrientation: 'vertical',
		legendCellSize: 20,
		displayLegend: false,
		label: {
			position: "top"
		}
	});

	$('.graph-label').click(function (argument) {
		var classes = $(this).parent().attr('class').split(' ');
		var mes = parseInt(classes[1].split('_')[1]) - 1;
		var anho = parseInt(classes[2].split('_')[1]);
		var fecha = new Date(anho, mes, 1);
		agenda.fullCalendar('gotoDate', fecha);
		agenda.fullCalendar('changeView', 'month');
	});

	$("#top-button-group > [class*='fc-button'], #download-container > [class*='fc-button']").hover(function () {
		$(this).toggleClass('fc-state-hover');
	});

	completedCalls += 1;
	if (completedCalls > 1) spinner.stop();
}


/**
Dibuja el componente agenda de la visualización
**/
function drawAgenda() {

	var tooltip = $('<div/>').qtip({
		id: 'fullcalendar',
		prerender: true,
		content: {
			text: ' ',
			title: {
				button: true
			}
		},
		position: {
			my: 'bottom center',
			at: 'top center',
			target: 'mouse',
			viewport: $('#fullcalendar'),
			adjust: {
				mouse: false,
				scroll: false
			}
		},
		show: false,
		hide: false,
		style: 'qtip-light'
	}).qtip('api');

	/*
		Initialize fullCalendar and store into variable.
		Why in variable?
		Because doing so we can use it inside other function.
		In order to modify its option later.
	*/
	var agenda = $('#agenda').fullCalendar(
		{
			/*
				header option will define our calendar header.
				left define what will be at left position in calendar
				center define what will be at center position in calendar
				right define what will be at right position in calendar
			*/
			header:
			{
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},

			hiddenDays: [0],// hide Tuesdays and Thursdays

			/*
				defaultView option used to define which view to show by default,
				for example we have used agendaWeek.
			*/
			defaultView: 'agendaDay',
			/*
				editable: true allow user to edit events.
			*/
			editable: false,
			/*
				events is the main option for calendar.
				for demo we have added predefined events in json object.
			*/
			timeFormat: {
				month: 'h:mm',
			},
			events: function (start, end, timezone, callback) {
				clearEventTable();
				var instituciones = $('#institucion').select2('val').toString();
				var categorias = $('#categoria').select2('val').toString();
				var modalidades = $('#modalidad').select2('val').toString();
				var desdeFilter = parseDate($('#desde').val());
				var desde = (desdeFilter || start).toJSON().substring(0, 10);
				var hastaFilter = parseDate($('#hasta').val());
				var hasta = (hastaFilter || end.add(-1, 'days')).toJSON().substring(0, 10);
				var events = [];
				if (completedCalls > 1) drawSpinner();
				dataService.getEventos(categorias, instituciones, modalidades, desde, hasta, function (data) {
					groups = groupEvents(data, desde, hasta);
					groups.forEach(function (g) {
						events.push({
							title: g.descripcion,
							start: g.fecha_entrega_oferta, // will be parsed
							end: moment(g.fecha_entrega_oferta).add(1, 'hours'),
							slug: g.slug,
							hashedId: g.id,
							events: g.events,
							backgroundColor: eventColor(g),
							borderColor: eventColor(g),
							textColor: eventTextColor(g)
						});
					});
					completedCalls += 1;
					if (completedCalls > 1) spinner.stop();
					callback(events);
				});
			},

			eventClick: function (data, event, view) {
				drawEventTable(data.events);
				if (window != window.top) {
					$(parent.document).scrollTop($("#lista-eventos").offset().top);
				}
				$(document).scrollTop($("#lista-eventos").offset().top);
			}

		});

	return agenda;

}

function eventColor(event) {
	var eventColors = ['#f5e7bf', '#f3da96', '#f4ae53', '#f97821', '#fe0516'];
	var i = eventIndex(event);
	return eventColors[i];
}

function eventTextColor(event) {
	/*var textColor = '#333';
	var i = eventIndex(event);
	if(i === 0){
		textColor = '#333';
	}*/
	return '#333';
}

function eventIndex(event) {
	var limits = [5, 15, 25, 35];
	var found = false;
	var i = 0;

	while (!found) {
		//extremos
		if ((i === 0 && event.cantidad < limits[i]) || i === limits.length) {
			found = true;
		} else if (event.cantidad >= limits[i - 1] && event.cantidad <= limits[i]) {
			found = true;
		} else {
			i++;
		}
	}
	return i;
}

function groupEvents(events, desde, hasta) {
	groupsByHour = {};
	pivot = moment(desde);
	limit = moment(hasta).add(1, 'days'); //La API toma hasta como un límite inclusive
	while (pivot.toDate() < limit.toDate()) {
		groupsByHour[pivot.toDate()] = {
			count: 0,
			events: []
		};
		pivot = pivot.add(1, 'hour');
	}

	events.forEach(function (e) {
		var group = groupsByHour[moment(e.fecha_entrega_oferta).startOf('hour').toDate()];
		group.count += 1;
		group.events.push(e);
	});

	groups = [];
	for (var k in groupsByHour) {
		if (groupsByHour.hasOwnProperty(k) && groupsByHour[k].count > 0) {
			var label = 'llamado';
			if (groupsByHour[k].count > 1) {
				label += 's';
			}

			groups.push({
				descripcion: groupsByHour[k].count.toString() + ' ' + label,
				fecha_entrega_oferta: k,
				cantidad: groupsByHour[k].count,
				events: groupsByHour[k].events
			});
		}
	}
	return groups;
}

function clearEventTable() {
	if ($.fn.DataTable.isDataTable('#lista-eventos')) {
		$('#lista-eventos').DataTable().clear().destroy();
		$('#lista-eventos').html('');
		$('#lista-eventos-title').toggle();
	}
}

function drawEventTable(events) {
	clearEventTable();
	$('#lista-eventos-title').toggle();
	var rows = events.map(function (e) {
		return [e.fecha_entrega_oferta, e.descripcion, e.id]
	});
	$('#lista-eventos').dataTable({
		"data": rows,
		"columns": [
			{
				"title": "Fecha de Entrega de Ofertas",
				"width": "20%"
			},
			{
				"title": "Descripción",
				"width": "60%"
			},
			{
				"title": "Enlaces",
				"width": "20%"
			}
		],
		"language": {
			"sProcessing": "Procesando...",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sZeroRecords": "No se encontraron resultados",
			"sEmptyTable": "Ningún dato disponible en esta tabla",
			"sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix": "",
			"sSearch": "Buscar:",
			"sUrl": "",
			"sInfoThousands": ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst": "Primero",
				"sLast": "Último",
				"sNext": "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		},
		"columnDefs": [
			{
				"render": function (data, type, row) {
					var content = '<div style="text-align:center;float:right;margin-left:10px;"><a href="' + dataService.getLlamadoUrl(data) + '" target="_blank"><img src="./images/online.png" alt="Ir a página web"></a>' +
						'<a href="' + dataService.getLlamadoUrl(data) + '" target="_blank"><p>Página Web</p></a></div>'
					return content;
				},
				"targets": 2
			},
			{
				"render": function (data, type, row) {
					return moment(data).format("DD-MM-YYYY HH:mm:ss");
				},
				"targets": 0
			},
			{
				bSortable: false,
				aTargets: [-1]
			}
		],
		"bAutoWidth": false
	});

	$('a.jsonld').click(function (e) {
		e.preventDefault();
		var url = $(this).attr('href');
		var newWindow = window.open(url);
		dataService.getJsonLd(url, function (llamado) {
			newWindow.document.body.innerHTML = JSON.stringify(llamado);
		});
	});
}

function getJsonLd() { }

/**
Dibuja un componente de selección de fechas de acuerdo al id que recibe como parámetro.
**/
function drawDatePicker(id) {
	var locale = {
		previousMonth: 'Mes Anterior',
		nextMonth: 'Mes Siguiente',
		months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb']
	};

	return new Pikaday({
		field: $(id)[0],
		i18n: locale,
		format: 'DD-MM-YYYY',
		minDate: new Date(2019, 7, 1)
	});
}

/**
Dibuja el botón para hacer embedding de la visualización.
**/
function drawEmbedButton() {
	var embed = $('#download').qtip({
		prerender: true,
		content: {
			text: '<div class="modal-content"> \
                <div class="modal-header"> \
                    <h3 class="modal-title">Descarga</h3> \
                </div> \
                <hr> \
                <div class="modal-body"> \
                    <p>Puedes incluir la visualización en tu sitio copiando y pegando el siguiente código:</p> \
                    <textarea id="embedcode" style="width:95%;margin-top:10px;" rows="5">&lt;iframe src="http://' + location.hostname + '/datos/assets/index-min.html" style="width: 90%; height: 900px; border: none; padding: 0; margin: 0;"&gt;&lt;/iframe&gt;</textarea> \
                </div> \
            </div>',
			title: {
				button: true
			}
		},
		show: false,
		hide: false,
		style: 'qtip-light'
	}).qtip('api');

	$('#download').click(function (event) {
		embed.reposition(event).show(event);
	});
}

function drawSpinner() {
	if (!spinner) {
		spinner = new Spinner({
			lines: 13,
			length: 20,
			width: 10,
			radius: 30,
			color: 'rgb(243, 218, 150)'
		});
	}

	var target = document.getElementById('grid');
	spinner.spin(target);
}

/** Función principal que construye la visualización **/
$(function () {
	var desde = drawDatePicker('#desde');
	var hasta = drawDatePicker('#hasta');
	drawSpinner();

	$("#categoria").select2({
		placeholder: "Haga clic para seleccionar una categoría"
	});
	$("#institucion").select2({
		placeholder: "Haga clic para seleccionar una institución"
	});
	$("#modalidad").select2({
		placeholder: "Haga clic para seleccionar una modalidad"
	});
	dataService.getCategorias(loadCategorias);
	dataService.getInstituciones(loadInstituciones);
	dataService.getModalidades(loadModalidades);
	drawEmbedButton();
	var agenda = drawAgenda();
	$('#categoria, #institucion, #modalidad, #desde, #hasta').change(function () {
		clearEventTable();
		loadChart(agenda);
		agenda.fullCalendar('refetchEvents');
	});
	loadChart(agenda);


});
