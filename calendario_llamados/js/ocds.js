$("#myTab a").click(function(e) {
	e.preventDefault();
	$(this).tab('show');
});

function createTables(url) {
	$.when(getFilesList(url)).done(function(data){
		var jsonData = JSON.parse(data);
		_.each(jsonData, function(value, index) {
			_.each(value, function(val, ind) {
				$("#table" + index).append(
						"<tr><td>" + val + "</td><td>"
								+ "<a href='" + url + "ocds/" + index + "/" 
								+ val + "'><i class='iconos dncp-json-wide-icon naranja'></i>JSON</a>"
								+ "</td></tr>");
			});
		});
	});	

}

function getFilesList(urlBase) {
    return $.ajax({
           		url : urlBase + "/ocds/data",
           		type : "get"
           	});
}
