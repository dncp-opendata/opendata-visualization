//var urlBase = "http://localhost:9001/datos/data/";
var loc = "API_BASE_HOST_URL";
var urlBase = loc + "/datos/api/v3/doc/data/";

var planificacionColumns = [
	"Id", "Id llamado", "Nombre licitación",
	"Convocante", "Categoría", "Tipo procedimiento", "Fecha estimada",
	"Objeto licitación"];
var	convocatoriaColumns = [
		"Id", "Id llamado", "Nombre licitación", "Convocante",
		"Categoría", "Tipo procedimiento", "Estado de la convocatoria",
		"Fecha de apertura de ofertas", "Sistema de adjudicación"];
var	adjudicacionColumns = [
		"Id", "Id llamado", "Nombre licitación", "Convocante",
		"Categoría", "Tipo procedimiento", "Estado de la adjudicación",
		"Monto adjudicado", "Fecha de publicación", "Razon social", "Ruc"];
var	contratoColumns = ["Id",
		"Id llamado", "Nombre licitación", "Convocante", "Código contratación", "Fecha firma contrato", "Monto total",
		"Vigencia contrato",
		"Moneda"];
catalogoColumns = ["Codigo", "Nombre", "Nivel 1", "Nivel 2", "Nivel 3", "Nivel 4", "Codigo Niv.4"];
proveedoresColumns = ["Razon Social", "RUC", "Tamaño", "Tipo de Entidad Legal"];

var columnData = {
	// para planificacion
	'Id': 'ocid',
	'Id llamado': 'ocid',
	'Nombre licitación': 'tender.title',
	'Convocante': 'buyer.name',
	'Categoría': 'tender.mainProcurementCategoryDetails',
	'Tipo procedimiento': 'tender.procurementMethodDetails',
	'Fecha estimada': 'planning.estimatedDate',
	'Objeto licitación': 'tender.mainProcurementCategoryDetails',
	// para convocatoria
	'Estado de la convocatoria': 'tender.statusDetails',
	'Fecha de apertura de ofertas': 'tender.tenderPeriod.endDate',
	'Sistema de adjudicación': 'tender.awardCriteriaDetails',
	// para adjudicacion
	'Estado de la adjudicación': 'awards.statusDetails',
	'Monto adjudicado': 'awards.value.amount',
	'Fecha de publicación': 'date',
	// para contratos
	'Código contratación': 'contracts.id',
	'Razon social': 'awards.suppliers.0.name',
	'Ruc': 'awards.suppliers.0.id',
	'Fecha firma contrato': 'contracts.dateSigned',
	'Monto total': 'contracts.value.amount',
	'Vigencia contrato': 'contracts.period.durationInDays',
	'Moneda': 'contracts.value.currency',
	// para modificaciones
	'Id contrato': 'contracts.id',
	//para catalogo
	'Codigo': 'id',
	'Nombre': 'nombre',
	'Nivel 1': 'n1_nombre',
	'Nivel 2': 'n2_nombre',
	'Nivel 3': 'n3_nombre',
	'Nivel 4': 'n4_nombre',
	'Codigo Niv.4': 'n4_codigo',
	// para proveedores
	'Razon Social': 'name',
	'RUC': 'identifier.id',
	'Tamaño': 'details.size',
	'Tipo de Entidad Legal': 'details.legalEntityTypeDetail'

};

var columnRenderers = {

	'Fecha estimada': function (data, type, full, meta) {
		return format_date(data, type);
	},
	'Fecha de apertura de ofertas': function (data, type, full, meta) {
		return format_date(data, type);
	},
	'Fecha de publicación': function (data, type, full, meta) {
		return format_date(data, type);
	},
	'Fecha firma contrato': function (data, type, full, meta) {
		return format_date(data, type);
	},
	'Monto adjudicado': function (data, type, full, meta) {
		console.log(data);
		return format_amount(data);
	},
	'Monto total': function (data, type, full, meta) {
		return format_amount(data);
	},
	'Monto': function (data, type, full, meta) {

		return format_amount(data);
	}
};

function format_amount(data) {
	if (data)
		return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function format_date(data, type) {
	var date = moment(data);
	if (type === "display" && date.isValid()) {
		return date.format('DD/MM/YYYY');
	}
	return data;
}

function init_config(modulo) {
	/* Por medio del modulo obtener las columnas correspondientes */
	var tableColumns;

	if (modulo === "planificaciones") {
		tableColumns = planificacionColumns;
	} else if (modulo === "convocatorias") {
		tableColumns = convocatoriaColumns;
	} else if (modulo === "adjudicaciones") {
		tableColumns = adjudicacionColumns;
	} else if (modulo === "contratos") {
		tableColumns = contratoColumns;
	} else if (modulo === "modificaciones") {
		tableColumns = modificacionColumns;
	} else if (modulo === "catalogo") {
		tableColumns = catalogoColumns;
			} else if (modulo === "proveedores") {
		tableColumns = proveedoresColumns;
	} else {
		tableColumns = [];
	}

	var columnCount = tableColumns.length;

	return columns = tableColumns.map(function (c, i) {
		return {
			"title": c,
			"data": columnData[c],
			"visible": (i < columnCount),
			"defaultContent": "",
			"render": columnRenderers[c]
		};
	});
}