/*
* @author Rodrigo Parra
* @copyright 2014 Governance and Democracy Program USAID-CEAMSO
* @license http://www.gnu.org/licenses/gpl-2.0.html
*
* USAID-CEAMSO
* Copyright (C) 2014 Governance and Democracy Program
* http://ceamso.org.py/es/proyectos/20-programa-de-democracia-y-gobernabilidad
*
----------------------------------------------------------------------------
* This file is part of the Governance and Democracy Program USAID-CEAMSO,
* is distributed as free software in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. You can redistribute it and/or modify it under the
* terms of the GNU Lesser General Public License version 2 as published by the
* Free Software Foundation, accessible from <http://www.gnu.org/licenses/> or write
* to Free Software Foundation (FSF) Inc., 51 Franklin St, Fifth Floor, Boston,
* MA 02111-1301, USA.
---------------------------------------------------------------------------
* Este archivo es parte del Programa de Democracia y Gobernabilidad USAID-CEAMSO,
* es distribuido como software libre con la esperanza que sea de utilidad,
* pero sin NINGUNA GARANTÍA; sin garantía alguna implícita de ADECUACION a cualquier
* MERCADO o APLICACION EN PARTICULAR. Usted puede redistribuirlo y/o modificarlo
* bajo los términos de la GNU Lesser General Public Licence versión 2 de la Free
* Software Foundation, accesible en <http://www.gnu.org/licenses/> o escriba a la
* Free Software Foundation (FSF) Inc., 51 Franklin St, Fifth Floor, Boston,
* MA 02111-1301, USA.
*/

/**
Servicios de acceso a datos para la visualización del calendario de llamados
**/
var dataService = new function () {
   // var base = 'http://localhost:9001/datos';
    var loc = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '')
    var base = loc + "/datos";
    var serviceBase = base + '/viz/';
    var serviceAuth = 'https://www.contrataciones.gov.py/datos/api/oauth/';
    var requestToken = 'NDcxMjc5YzQtNjI1Yi00YjNiLWE4Y2QtYTBlN2VlOGRhZThlOjE0MzE1OTk5LTg2ZDQtNDYyZS1iMDRlLWM3Y2VjNzkyZWU2Zg==';
    /** Obtiene la lista de categorías posibles de un llamado, ejecutando
    el callback en caso de éxito. **/
    getCategorias = function (callback) {
      $.getJSON(serviceBase + 'categorias',
        function (data) {
            callback(data);
        });
    };

    /** Obtiene la lista de instituciones convocantes posibles de un llamado, ejecutando
    el callback en caso de éxito. **/
    getInstituciones = function (callback) {
      $.getJSON(serviceBase + 'entidades',
        function (data) {
            callback(data);
        });
    };

    /** Obtiene la lista de modalidades posibles de un llamado, ejecutando
    el callback en caso de éxito. **/
    getModalidades = function (callback) {
        $.getJSON(serviceBase + 'modalidades',
          function (data) {
              callback(data);
          });
    };

    /** Obtiene el resumen de llamados por fecha de entrega de ofertas, considerando los filtros
    de la visualización, ejecutando el callback en caso de éxito. **/
    getCalendario = function (categorias, instituciones, modalidades, desde, hasta, callback) {
      var params = {};

      if(instituciones){
        params.entidades = instituciones;
      }
      if(modalidades){
        params.modalidades = modalidades;
      }
      if(categorias){
        params.categorias = categorias;
      }
      if(desde){
        params.fecha_desde = desde;
      }
      if(hasta){
        params.fecha_hasta = hasta;
      }

      $.getJSON(serviceBase + 'calendario', params).done(function (data) {
          callback(data);
      }).fail(function( jqxhr, textStatus, error ) {
          callback([]);
      })
    };

    /** Obtiene la lista de llamados por fecha de entrega de ofertas, considerando los filtros
    de la visualización, ejecutando el callback en caso de éxito. **/
    getEventos = function (categorias, instituciones, modalidades, desde, hasta, callback) {
      var params = {};

      if(instituciones){
        params.entidades = instituciones;
      }
      if(modalidades){
        params.modalidades = modalidades;
      }
      if(categorias){
        params.categorias = categorias;
      }
      if(desde){
        params.fecha_desde = desde;
      }
      if(hasta){
        params.fecha_hasta = hasta;
      }
      $.getJSON(serviceBase + 'eventos', params).done(function (data) {
          callback(data);
      }).fail(function( jqxhr, textStatus, error ) {
          callback([]);
      })
    };

    /** Retorna la URL de un llamado, de acuerdo a su id encriptado, en el portal de la DNCP. **/
    getLlamadoUrl = function(id){
      var baseUrl = 'https://www.contrataciones.gov.py/licitaciones/convocatoria/';
      var llamadoParam = encodeURIComponent(id)+'.html';
      return baseUrl + llamadoParam;
    }

    getAccessToken = function(callback){
      var currentToken = keyStorage.load('access_token');
      if(currentToken){
        console.log("Token con localStorage");
        callback(currentToken);
      }else{
        $.ajax({
          dataType: "json",
          type: "POST",
          url: serviceAuth + 'token',
          headers: {'Authorization': 'Basic ' + requestToken},
          success: function (data) {
            console.log("Token con AJAX");
            keyStorage.save("access_token", data.access_token, 14);
            callback(data.access_token);
          }
        });
      }
    }

    getJsonLd = function(url, callback){
      getAccessToken(function(accessToken){
        $.ajax({
          dataType: "json",
          type: "GET",
          url: url,
          headers: {'Authorization': 'Bearer ' + accessToken},
          success: function (data) {
            callback(data);
          }
        });
      });
    }

    return {
        getCategorias: getCategorias,
        getInstituciones: getInstituciones,
        getModalidades: getModalidades,
        getCalendario: getCalendario,
        getEventos: getEventos,
        getLlamadoUrl: getLlamadoUrl,
        getAccessToken: getAccessToken,
        getJsonLd: getJsonLd
    };
} ();

var keyStorage = new function() {
  save = function(key, jsonData, expirationMin){
    var expirationMS = expirationMin * 60 * 1000;
    var record = {value: JSON.stringify(jsonData), timestamp: new Date().getTime() + expirationMS};
    try{
      localStorage.setItem(key, JSON.stringify(record));
    }catch(e){
      alert("Por favor actualice su navegador");
    }
    return jsonData;
  }

  load = function(key){
    try{
      var record = JSON.parse(localStorage.getItem(key));
    }catch(e){
      alert("Por favor actualice su navegador");
    }
    if (!record){return false;}
    return (new Date().getTime() < record.timestamp && JSON.parse(record.value));
  }

  return {
    save: save,
    load: load
  }
} ();
