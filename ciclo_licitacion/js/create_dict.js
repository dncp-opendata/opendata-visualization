function onChangeVersion(){
	var selectVersiones = document.getElementById("versiones");//$("#versiones");
	console.log(selectVersiones);
	var selectedVersion = selectVersiones.options[selectVersiones.selectedIndex].value;
	console.log("selectedVersion");
	console.log(selectedVersion);
	$("#tabla tbody").children().detach();
	createClasesTable(selectedVersion);
	var a = document.getElementById('descarga-owl'); //or grab it by tagname etc
	var selectedUrl = $(selectVersiones.options[selectVersiones.selectedIndex]).data('url');
	a.href = selectedUrl + "def/dncp.owl";
}

function setDefaultOwlUrl(){
	var selectVersiones = document.getElementById("versiones");
	var selectedUrl = $(selectVersiones.options[0]).data('url');
	//console.log(selectedUrl);
	var a = document.getElementById('descarga-owl');
	a.href = selectedUrl + "def/dncp.owl";
}


function createClasesTable(data) {
	var selectVersiones = document.getElementById("versiones");//$("#versiones");
	var selectedId = $(selectVersiones.options[selectVersiones.selectedIndex]).data('id');
	console.log("id");
	console.log(selectedId);
	var jsonData = JSON.parse(data);
		_.each(jsonData, function(value, index) {
			var clase = value['Clases'] != undefined ? value['Clases'] : " ";
			var label = value['Label: Español']  != undefined ? value['Label: Español'] : " ";
			var classRef = '<a href="' + "/datos/" + selectedId + "/" + "def/" + clase +'">' + clase + '</a>';
			//var classRef = '<a href="' + "/datos/def/" + clase +'">' + clase + '</a>';
			var descripcion = value['Descripcion: Español'] != undefined ? value['Descripcion: Español'] : " ";
			var claseEq = value['Clase equivalente'] != undefined ? value['Clase equivalente'] : " ";
			$("#tabla").append(
					"<tr><td>" + classRef + "</td>" +
					"<td>" + label + "</td>" +
					"<td>" + descripcion + "</td>" +
					"<td>" + claseEq + "</td></tr>");
	});

}

function createPropiedadesTable(data) {
	var jsonData = JSON.parse(data);
		//console.log(jsonData);
		_.each(jsonData, function(value, index) {
			var prop = value['propiedad en json-ld'] != undefined ? value['propiedad en json-ld'] : "";
			var nombre = value['Nombre de la propiedad'] != undefined ? value['Nombre de la propiedad'] : "";
			var labelEs = value['Label: ESPAÑOL'] != undefined ? value['Label: ESPAÑOL'] : "";
			var labels = value['Label Alternativos: ESPAÑOL, separados por comas'] != undefined ? value['Label Alternativos: ESPAÑOL, separados por comas'] : "";
			var ejemplo = value['Ejemplo'] != undefined ?  value['Ejemplo'] : "";
			var tipo = value['Tipo de datos'] != undefined ? value['Tipo de datos'] : "";
			var cardinalidad = value['Cardinalidad (single or multivalue)'] != undefined ? value['Cardinalidad (single or multivalue)'] : "";
			var formato = value['Formato, si aplica, EJ, para las fechas'] != undefined ? value['Formato, si aplica, EJ, para las fechas'] : "";
			var restricciones = value['Restricciones (incluyendo posibles valores separados por comas)'] != undefined ? value['Restricciones (incluyendo posibles valores separados por comas)'] : "";
			var descripcion = value['Descripcion: Español'] != undefined ? value['Descripcion: Español'] : "";
			var nose = value['Restricciones (incluyendo posibles valores separados por comas)'] != undefined ? value['propiedad en json-ld'] : "";
			var propEq = value['Propiedad Equivalente'] != undefined ? value['Propiedad Equivalente'] : "";
			$("#tabla").append(
					"<tr><td>" + prop  + "</td>" +
					"<td>" + nombre + "</td>" +
					"<td>" + labelEs + "</td>" +
					"<td>" + labels + "</td>" +
					"<td>" + ejemplo + "</td>" +
					"<td>" + tipo + "</td>" +
					"<td>" + cardinalidad + "</td>" +
					"<td>" + formato + "</td>" +
					"<td>" + restricciones + "</td>" +
					"<td>" + descripcion + "</td>" +
					"<td>" + propEq + "</td></tr>");
	});
		
		

}