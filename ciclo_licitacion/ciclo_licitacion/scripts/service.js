'use strict';
/*global Modernizr:true */
var CGraph = CGraph || {};

CGraph.keyStorage = (function() {
  var save = function(key, jsonData, expirationMin){
    var expirationMS = expirationMin * 60 * 1000;
    var record = {value: JSON.stringify(jsonData), timestamp: new Date().getTime() + expirationMS};
    if(Modernizr.localstorage){
      localStorage.setItem(key, JSON.stringify(record));
    }else{
      window.alert('Por favor actualice su navegador save');
    }
    return jsonData;
  };

  var load = function(key){
    var record;
    if(Modernizr.localstorage){
      record = JSON.parse(localStorage.getItem(key));
    }else{
      window.alert('Por favor actualice su navegador load');
    }
    if (!record){return false;}
    return (new Date().getTime() < record.timestamp && JSON.parse(record.value));
  };

  return {
    save: save,
    load: load
  };
})();

CGraph.dataService = (function() {
  var cont = 0;
  var loc = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '')
  loc = "https://www.contrataciones.gov.py";
  var base = loc + "/datos";
  var serviceBase = base + '/api/doc/';
  var serviceAuth = base + '/api/oauth/';
  var requestToken = 'NDE1NWRjNzItMzE2My00NDUxLWExYTYtYmRhNzJjNDIyMWM0OmY3YmYyMTM3LTY4NWYtNDU5ZS1iMGIxLTIwY2I5MmJjYTNjYQ==';

  var getAccessToken = function(callback){
    cont++;
    //console.log(cont);
    var promise, self = this;
    var currentToken = CGraph.keyStorage.load('access_token');
    if(currentToken){
      //console.log('Token con localStorage');
      promise = callback(currentToken);
      return promise;
    }else{
      return $.ajax({
        dataType: 'json',
        type: 'POST',
        url: serviceAuth + 'token',
        headers: {'Authorization': 'Basic ' + requestToken},
        success: function (data) {
          //console.log('Token con AJAX');

        }
      }).then(function(data){
        CGraph.keyStorage.save('access_token', data.access_token, 14);
        return callback(data.access_token);
      });
    }
    //console.log(pendingRequests);
  };

  var getPlanificacion = function(id){
    return this.getAccessToken(function(accessToken){
      return $.ajax({
        dataType: "json",
        type: "GET",
        url: serviceBase + 'planificaciones/' + id,
        headers: {'Authorization': 'Bearer ' + accessToken},
        dataFilter: function(data){
          var jsonData = JSON.parse(data)['@graph'][0];
          return JSON.stringify(jsonData);
        }
      });
    });
  };

  var getConvocatorias = function(url){
    url = url.replace('https://www.contrataciones.gov.py/datos', base); //Dev only
    return this.getAccessToken(function(accessToken){
      return $.ajax({
        dataType: "json",
        type: "GET",
        url: url,
        headers: {'Authorization': 'Bearer ' + accessToken},
        dataFilter: function(data){
          var jsonData = JSON.parse(data)['@graph'][0].convocatoria.list;
          return JSON.stringify(jsonData);
        }
      });
    });
  }

  var getConvocatoria = function(id){
    return this.getAccessToken(function(accessToken){
      return $.ajax({
        dataType: "json",
        type: "GET",
        url: serviceBase + 'convocatorias/' + id,
        headers: {'Authorization': 'Bearer ' + accessToken},
        dataFilter: function(data){
          var jsonData = JSON.parse(data)['@graph'][0];
          return JSON.stringify(jsonData);
        }
      });
    });
  }

  var getAdjudicaciones = function(url){
    url = url.replace('https://www.contrataciones.gov.py/datos', base); //Dev only
    return this.getAccessToken(function(accessToken){
      return $.ajax({
        dataType: "json",
        type: "GET",
        url: url,
        headers: {'Authorization': 'Bearer ' + accessToken},
        dataFilter: function(data){
          var jsonData = JSON.parse(data)['@graph'][0].adjudicacion.list;
          return JSON.stringify(jsonData);
        }
      });
    });
  }

  var getAdjudicacion = function(id){
    return this.getAccessToken(function(accessToken){
      return $.ajax({
        dataType: "json",
        type: "GET",
        url: serviceBase + 'adjudicaciones/' + id,
        headers: {'Authorization': 'Bearer ' + accessToken},
        dataFilter: function(data){
          var jsonData = JSON.parse(data)['@graph'][0];
          return JSON.stringify(jsonData);
        }
      });
    });
  }

  var getContratos = function(url){
    url = url.replace('https://www.contrataciones.gov.py/datos', base); //Dev only
    return this.getAccessToken(function(accessToken){
      return $.ajax({
        dataType: "json",
        type: "GET",
        url: url,
        headers: {'Authorization': 'Bearer ' + accessToken},
        dataFilter: function(data){
          var jsonData = JSON.parse(data)['@graph'][0].contrato.list;
          return JSON.stringify(jsonData);
        }
      });
    });

  }

  var getContrato = function(id){
    return this.getAccessToken(function(accessToken){
      return $.ajax({
        dataType: "json",
        type: "GET",
        url: serviceBase + 'contratos/' + id,
        headers: {'Authorization': 'Bearer ' + accessToken},
        dataFilter: function(data){
          var jsonData = JSON.parse(data)['@graph'][0];
          return JSON.stringify(jsonData);
        }
      });
    });
  }

  var getModificacionesContrato = function(url){
    url = url.replace('https://www.contrataciones.gov.py/datos', base); //Dev only
    return this.getAccessToken(function(accessToken){
      return $.ajax({
        dataType: "json",
        type: "GET",
        url: url,
        headers: {'Authorization': 'Bearer ' + accessToken},
        dataFilter: function(data){
          var jsonData = JSON.parse(data)['@graph'][0]['modificacion_contrato'].list;
          return JSON.stringify(jsonData);
        }
      });
    });

  }

  var getModificacionContrato = function(id){
    return this.getAccessToken(function(accessToken){
      return $.ajax({
        dataType: "json",
        type: "GET",
        url: serviceBase + 'modificaciones-contrato/' + id,
        headers: {'Authorization': 'Bearer ' + accessToken},
        dataFilter: function(data){
          var jsonData = JSON.parse(data)['@graph'][0];
          return JSON.stringify(jsonData);
        }
      });
    });
  }

  var getLicitacion = function(nro){
    return this.getAccessToken(function(accessToken){
      return $.ajax({
        dataType: "json",
        type: "GET",
        url: serviceBase + 'buscadores/licitaciones',
        headers: {'Authorization': 'Bearer ' + accessToken},
        data: {'nro_nombre_llamado': nro, 'limit': 1, 'offset': 0},
        dataFilter: function(data){
          var jsonData = JSON.parse(data)['@graph'][0].list[0];
          return JSON.stringify(jsonData);
        }
      });
    });
  }

  var _fetchAll = function(baseCollection, service, attr){
    var self = this;
    baseCollection = _.flatten(baseCollection);
    var result = [];
    var d = $.Deferred();
    if(baseCollection.length === 0) d.resolve([]);
    var promises = _.map(baseCollection, function(e){ return service.call(self, e[attr]); });
    var resolve = _.after(promises.length, function(){ d.resolve(result); });
    _.each(promises, function(p){
      p.done(function(e, i){ result.push(e); }).always(resolve);
    });
    return d;
  }

  var fetchNodos = function(id){
    var licitacionResult, planificacionResult, convocatoriasResult, adjudicacionesResult, contratosResult, modificacionesResult;
    var self = this;
    return self.getLicitacion(id)
    .then(function(licitacion){
        licitacionResult = licitacion;
       return self.getPlanificacion(licitacion.planificacion_id);
    })
    .then(function(pac){
      planificacionResult = pac;
      return _fetchAll.call(self, [pac], self.getConvocatorias, 'convocatorias');
    })
    .then(function(convocatorias){ return _fetchAll.call(self, convocatorias, self.getConvocatoria, 'id'); })
    .then(function(convocatorias){
      convocatoriasResult = convocatorias;
      return _fetchAll.call(self, convocatorias, self.getAdjudicaciones, 'adjudicaciones');
    }).then(function(adjudicaciones){ return _fetchAll.call(self, adjudicaciones, self.getAdjudicacion, 'id'); })
    .then(function(adjudicaciones){
      adjudicacionesResult = adjudicaciones;
      return _fetchAll.call(self, adjudicaciones, self.getContratos, 'proveedores_adjudicados');
    })
    .then(function(contratos){ return _fetchAll.call(self, contratos, self.getContrato, 'id'); })
    .then(function(contratos){
      contratosResult = contratos;
      return _fetchAll.call(self, contratos, self.getModificacionesContrato, 'modificaciones_contrato');
    })
    .then(function(modificaciones){ return _fetchAll.call(self, modificaciones, self.getModificacionContrato, 'id'); })
    .then(function(modificaciones){
      var d = $.Deferred();
      modificacionesResult = modificaciones;
      d.resolve(licitacionResult, planificacionResult, convocatoriasResult, adjudicacionesResult, contratosResult, modificacionesResult);
      return d;
    });
  };

  var fetchPlanificacionTree = function(id){
    return this.fetchNodos(id).then(function(planificacion, convocatorias, adjudicaciones, contratos, modificaciones){
      var d = $.Deferred();
      _.each(contratos, function(c){
        c.modificaciones_contrato = _.filter(modificaciones, function(m){ return m.contrato_id === c.id; });
      });
      _.each(adjudicacionesResult, function(a){
        a.contratos = _.filter(contratos, function(c){ return c.adjudicacion_id === a.id; });
      });
      _.each(convocatorias, function(c){
        c.adjudicaciones = _.filter(adjudicaciones, function(a){ return a.convocatoria_id === c.id; });
      });
      planificacion.convocatorias = convocatorias;
      d.resolve(planificacion);
      return d;
    });
  };

  return {
    getAccessToken : getAccessToken,
    getPlanificacion : getPlanificacion,
    getConvocatorias : getConvocatorias,
    getConvocatoria : getConvocatoria,
    getAdjudicaciones : getAdjudicaciones,
    getAdjudicacion : getAdjudicacion,
    getLicitacion : getLicitacion,
    getContratos : getContratos,
    getContrato : getContrato,
    getModificacionesContrato : getModificacionesContrato,
    getModificacionContrato : getModificacionContrato,
    fetchNodos : fetchNodos,
    fetchPlanificacionTree : fetchPlanificacionTree
  };
})();

CGraph.graphService = (function() {
  var getTree = function(id, truncate, maxNodes){
    truncate = true || truncate;
    maxNodes = 3 || maxNodes;
    console.time('fetchLlamado');
    return CGraph.dataService.fetchNodos(id)
      .then(function(licitacion, planificacion, convocatorias, adjudicaciones, contratos, modificaciones){
        var d = $.Deferred();
        var root = {
                    'name': 'Planificación',
                    'fechaEstimada': (moment(planificacion['fecha_estimada']).isValid()) ? 'Estimada ' + moment(planificacion['fecha_estimada'], 'YYYY-MM-DD').format('L') : '',
                    'nodeId': planificacion.id
                  }

        var nodosConvocatorias = _.map(convocatorias, function(c){
          return {
            'name': 'Convocatoria',
            'fechaPublicacion': (moment(c['fecha_publicacion'], 'YYYY-MM-DD').isValid()) ? moment(c['fecha_publicacion'], 'YYYY-MM-DD').format('L') : '',
            'estado': c.estado.nombre,
            'nodeId': c.id,
            'planificacionId': c['planificacion_id']
          }
        });

        var nodosAdjudicaciones = _.map(adjudicaciones, function(a){
          return {
            'name': 'Adjudicación',
            'fechaPublicacion': (moment(a['fecha_publicacion'], 'YYYY-MM-DD').isValid()) ? moment(a['fecha_publicacion'], 'YYYY-MM-DD').format('L') : '',
            'nodeId': a.id,
            'convocatoriaId': a['convocatoria_id']
          }
        });

        var nodosContratos = _.map(contratos, function(c){
          return {
            'name': 'Contrato',
            'fechaFirmaContrato': (moment(c['fecha_firma_contrato'], 'YYYY-MM-DD').isValid()) ? moment(c['fecha_firma_contrato'], 'YYYY-MM-DD').format('L') : '',
            'proveedor': c.proveedor['razon_social'],
            'estado': c.estado.nombre,
            'monto':  c['monto_adjudicado'].toLocaleString() + ' ' + c.moneda.nombre,
            'montoOriginal': c['monto_adjudicado'],
            'nodeId': c.id,
            'adjudicacionId': c['adjudicacion_id']
          }
        });
        var nodosModificaciones = _.map(modificaciones, function(m){
          //console.log(m);
          return {
            'name': m.tipo,
            'fecha': moment().format('L'),
            'estado': m.estado.nombre,
            'monto': (m.monto) ? m.moneda.codigo + ' ' + m['monto'].toLocaleString() : '',
            'nodeId': m.id,
            'contratoId': m['contrato_id']
          }
        });
        var longitudResto;

        _.each(nodosContratos, function(c){
          c.children = _(nodosModificaciones).filter(function(m){ return m.contratoId === c.nodeId; })
                                              .sortBy(function(m){ return moment(m.fecha, 'DD-MM-YYYY'); })
                                              .reverse()
                                              .value();
        });

        _.each(nodosAdjudicaciones, function(a){
          a.children = _(nodosContratos).filter(function(c){ return c.adjudicacionId === a.nodeId; })
                                          .sortBy(function(c){ return c.montoOriginal; })
                                          .reverse()
                                          .value();
          if(a.children.length > maxNodes){
            longitudResto = a.children.length - (maxNodes - 1)
            a.children = _.take(a.children, maxNodes - 1);
            a.children.push({
              'name': longitudResto.toString() + ' contratos más'
            });
          }
        });

        _.each(nodosConvocatorias, function(c){
          c.children = _(nodosAdjudicaciones).filter(function(a){ return a.convocatoriaId === c.nodeId; })
                                              .sortBy(function(a){ return moment(c.fechaPublicacion, 'DD-MM-YYYY'); })
                                              .reverse()
                                              .value();
        });

        root.children = _(nodosConvocatorias).sortBy(function(c){ return moment(c.fechaPublicacion, 'DD-MM-YYYY'); })
                                             .reverse()
                                             .value();


        d.resolve({
            tree: root,
            licitacion: licitacion
        });
        console.timeEnd('fetchLlamado');
        return d;
    });
  };

  return {
    getTree : getTree
  };
})();

/*$(document).ready(function(){
  CGraph.graphService.getTree('193399-adquisicion-scanner').then(function(llamadoTree){
    console.log(JSON.stringify(llamadoTree, null, 2));
  });
});*/
