'use strict';
/*global Modernizr:true */
var CGraph = CGraph || {};

CGraph.keyStorage = (function () {
  var save = function (key, jsonData, expirationMin) {
    var expirationMS = expirationMin * 60 * 1000;
    var record = { value: JSON.stringify(jsonData), timestamp: new Date().getTime() + expirationMS };
    if (Modernizr.localstorage) {
      localStorage.setItem(key, JSON.stringify(record));
    } else {
      window.alert('Por favor actualice su navegador save');
    }
    return jsonData;
  };

  var load = function (key) {
    var record;
    if (Modernizr.localstorage) {
      record = JSON.parse(localStorage.getItem(key));
    } else {
      window.alert('Por favor actualice su navegador load');
    }
    if (!record) { return false; }
    return (new Date().getTime() < record.timestamp && JSON.parse(record.value));
  };

  return {
    save: save,
    load: load
  };
})();

CGraph.dataService = (function () {
  var cont = 0;
  var loc = "API_BASE_HOST_URL";
  let base = loc + "/datos/api/v3/doc/visualizations/minimal/compiledRelease";
  var serviceBase = base;


  var getLicitacion = function (nro) {
    return $.ajax({
      dataType: "json",
      type: "GET",
      url: serviceBase + '/' + nro,
      dataFilter: function (data) {
        console.log(data);
        var jsonData = JSON.parse(data);
        return JSON.stringify(jsonData);
      }
    });
  }

  var _fetchAll = function (baseCollection, service, attr) {
    var self = this;
    baseCollection = _.flatten(baseCollection);
    var result = [];
    var d = $.Deferred();
    if (baseCollection.length === 0) d.resolve([]);
    var promises = _.map(baseCollection, function (e) { return service.call(self, e[attr]); });
    var resolve = _.after(promises.length, function () { d.resolve(result); });
    _.each(promises, function (p) {
      p.done(function (e, i) { result.push(e); }).always(resolve);
    });
    return d;
  }

  var fetchNodos = function (id) {
    var licitacionResult, planificacionResult, convocatoriasResult, adjudicacionesResult, contratosResult, modificacionesResult;
    var self = this;
    return self.getLicitacion(id)
      .then(function (licitacion) {
        licitacionResult = licitacion;
        licitacionResult['nombre_licitacion'] = licitacion.tender.title;
        licitacionResult['id_llamado'] = licitacion.ocid.replace('ocds-03ad3f-', '');
        licitacionResult['convocante'] = licitacion.tender.procuringEntity.name;
        licitacionResult['etapa'] = { nombre: licitacion.tender.statusDetails };
        planificacionResult = licitacion.planning;
        if(licitacion.previousTenders) {
            convocatoriasResult = licitacion.previousTenders;
        } else {
            convocatoriasResult = [];
        }
        var adjudicado = licitacion.tender;
        //adjudicado["datePublished"] = licitacion.tender.datePublished;
        convocatoriasResult.unshift(licitacion.tender);
        //console.log(convocatoriasResult);
        adjudicacionesResult = licitacion.awards;
        if (!adjudicacionesResult) {
            adjudicacionesResult = [];
        }
        adjudicacionesResult.forEach(function (d) {
        d.convocatoria_id = licitacion.tender.id;
      });
        contratosResult = licitacion.contracts;
        modificacionesResult = _.flatten(_.map(licitacion.contracts, function (c) {
          _.forEach(c.amendments, function (m) {
            m['contract_id'] = c.id;
          })
          return c.amendments
        }));
        var d = $.Deferred();
        d.resolve(licitacionResult, planificacionResult, convocatoriasResult, adjudicacionesResult, contratosResult, modificacionesResult);
        return d;
      });
  };

  var fetchPlanificacionTree = function (id) {
    console.log('Aca llama...');
    return this.fetchNodos(id).then(function (planificacion, convocatorias, adjudicaciones, contratos, modificaciones) {
      var d = $.Deferred();
      _.each(contratos, function (c) {
        c.modificaciones_contrato = _.filter(modificaciones, function (m) { return m.contrato_id === c.id; });
      });
      _.each(adjudicacionesResult, function (a) {
        a.contratos = _.filter(contratos, function (c) { return c.adjudicacion_id === a.id; });
      });
      _.each(convocatorias, function (c) {
        c.adjudicaciones = _.filter(adjudicaciones, function (a) { return a.convocatoria_id === c.id; });
      });
      planificacion.convocatorias = convocatorias;
      d.resolve(planificacion);
      return d;
    });
  };

  return {
    getLicitacion: getLicitacion,
    fetchNodos: fetchNodos,
    fetchPlanificacionTree: fetchPlanificacionTree
  };
})();

CGraph.graphService = (function () {
  var getTree = function (id, truncate, maxNodes) {
    truncate = true || truncate;
    maxNodes = 3 || maxNodes;
    console.time('fetchLlamado');
    return CGraph.dataService.fetchNodos(id)
      .then(function (licitacion, planificacion, convocatorias, adjudicaciones, contratos, modificaciones) {
        var d = $.Deferred();

        var root = {
          'name': 'Planificación',
          'fechaEstimada': (moment(planificacion['estimatedDate']).isValid()) ? 'Estimada ' + moment(planificacion['estimatedDate'], 'YYYY-MM-DD').format('L') : '',
          'nodeId': planificacion.id || 1
        };

        var nodosConvocatorias = _.map(convocatorias, function (c) {
          return {
            'name': 'Convocatoria',
            'fechaPublicacion': (moment(c['datePublished'], 'YYYY-MM-DD').isValid()) ? moment(c['datePublished'], 'YYYY-MM-DD').format('L') : '',
            'estado': c.statusDetails,
            'nodeId': c.id,
            'planificacionId': c['planificacion_id']
          }
        });

        var nodosAdjudicaciones = _.map(adjudicaciones, function (a) {
          return {
            'name': 'Adjudicación',
            'fechaPublicacion': (moment(a['date'], 'YYYY-MM-DD').isValid()) ? moment(a['date'], 'YYYY-MM-DD').format('L') : '',
            'nodeId': a.id,
            'convocatoriaId': a['convocatoria_id']
          }
        });

        var nodosContratos = _.map(contratos, function (c) {
          return {
            'name': 'Contrato',
            'fechaFirmaContrato': (moment(c['dateSigned'], 'YYYY-MM-DD').isValid()) ? moment(c['dateSigned'], 'YYYY-MM-DD').format('L') : '',
            'estado': c.statusDetails,
            'monto': c.value ? c.value.amount.toLocaleString() + ' ' + c.value.currency : '',
            'montoOriginal': c.value ? c.value.amount.toLocaleString() + ' ' + c.value.currency : '',
            'nodeId': c.id,
            'adjudicacionId': c.awardID,
              'proveedor': c['suppliers'][0].name
          }
        });
        var nodosModificaciones = _.map(modificaciones, function (m) {
            if (!m) {
                return []
            }
          return {
            'name': m.description,
            'fecha': (moment(m['date'], 'YYYY-MM-DD').isValid()) ? moment(m['date'], 'YYYY-MM-DD').format('L') : '',
            'estado': m.description,
            'monto': (m.monto) ? m.moneda.codigo + ' ' + m['monto'].toLocaleString() : '',
            'nodeId': m.id,
            'contratoId': m['contract_id']
          }
        });
        var longitudResto;

        _.each(nodosContratos, function (c) {

                c.children = _(nodosModificaciones).filter(function (m) {
                    return m.contratoId === c.nodeId;
                })
                    .sortBy(function (m) {
                        return moment(m.fecha, 'DD-MM-YYYY');
                    })
                    .reverse()
                    .value();

        });

        _.each(nodosAdjudicaciones, function (a) {
          a.children = _(nodosContratos).filter(function (c) { return c.adjudicacionId === a.nodeId; })
            .sortBy(function (c) { return c.montoOriginal; })
            .reverse()
            .value();
          if (a.children.length > maxNodes) {
            longitudResto = a.children.length - (maxNodes - 1)
            a.children = _.take(a.children, maxNodes - 1);
            a.children.push({
              'name': longitudResto.toString() + ' contratos más'
            });
          }
        });

        _.each(nodosConvocatorias, function (c) {
          c.children = _(nodosAdjudicaciones).filter(function (a) { return a.convocatoriaId === c.nodeId; })
            .sortBy(function (a) { return moment(c.fechaPublicacion, 'DD-MM-YYYY'); })
            .reverse()
            .value();
        });

        root.children = _(nodosConvocatorias).sortBy(function (c) { return moment(c.fechaPublicacion, 'DD-MM-YYYY'); })
          .reverse()
          .value();


        d.resolve({
          tree: root,
          licitacion: licitacion
        });
        return d;
      });
  };

  return {
    getTree: getTree
  };
})();

/*$(document).ready(function(){
  CGraph.graphService.getTree('193399-adquisicion-scanner').then(function(llamadoTree){
    console.log(JSON.stringify(llamadoTree, null, 2));
  });
});*/
