CGraph.treeBuilder = (function () {
  var margin = { top: 20, right: 120, bottom: 20, left: 120 },
    width = 1060 - margin.right - margin.left,
    height = 700 - margin.top - margin.bottom;

  var i = 0,
    duration = 750,
    root;

  var tree = d3.layout.tree()
    .size([height, width]);

  var diagonal = d3.svg.diagonal()
    .projection(function (d) { return [d.y, d.x]; });

  var svg, spinner;

  function setUp() {
    svg = d3.select("#viz-container").append("svg")
      .attr("class", "fondo-gris")
      .attr("width", width + margin.right + margin.left)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  };

  function showTree(id) {
    d3.select("svg").remove();
    var target = document.getElementById('viz-container');
    if (!spinner) {
      spinner = new Spinner({
        lines: 13,
        length: 20,
        width: 10,
        radius: 30,
        color: '#fff'
      });
    }
    $('#encabezado').html('');
    spinner.stop();
    spinner.spin(target);

    CGraph.graphService.getTree(id).then(function (result) {
      drawHeader(result.licitacion);
      drawTree(result.tree);
      spinner.stop();
    }, function (cause) {
      toastr.error('Ha ocurrido un error al obtener la licitación.');
      spinner.stop();
    });
  }

  function drawHeader(licitacion) {
    var source = $("#template-encabezado").html();
    var context = {
      nombre: licitacion['nombre_licitacion'],
      numero: licitacion['id_llamado'],
      convocante: licitacion.convocante.split('/')[0],
      etapa: licitacion.etapa.nombre
    };

    context.rojo = _.contains(['Cancelada', 'Anulada', 'Desierta'], context.etapa);
    context.naranja = _.contains(['Suspendida', 'Impugnada'], context.etapa);
    context.verde = _.contains(['Adjudicado', 'Ejecutada', 'Publicada'], context.etapa) || !(context.rojo || context.naranja);

    var content;
    template = Handlebars.compile(source);
    content = template(context);
    $('#encabezado').html(content);
  }

  function drawTree(llamadoTree) {
    setUp();
    root = llamadoTree;
    root.x0 = height / 2;
    root.y0 = 0;

    function collapse(d) {
      if (d.children && d.children.length > 0) {
        d._children = d.children;
        d._children.forEach(collapse);
        d.children = null;
      }
    }

    var singleNode = root.children.length === 0;
    //root.children.forEach(collapse);
    var trimmedHeight = update(root, singleNode);
    d3.select('svg').attr("height", trimmedHeight + margin.top + margin.bottom);

  }


  function contentByNode(nodo) {
    var source, template, context, content;
    switch (nodo.depth) {
      case 0:
        source = $("#nodo-planificacion").html();
        context = { titulo: nodo.name, fecha: nodo.fechaEstimada };
        break;

      case 1:
        source = $("#nodo-convocatoria").html();
        context = { titulo: nodo.name, fecha: nodo.fechaPublicacion, estado: nodo.estado };
        break;

      case 2:
        source = $("#nodo-adjudicacion").html();
        context = { titulo: nodo.name, fecha: nodo.fechaPublicacion, proveedor: nodo.proveedor };
        break;

      case 3:
        //Placeholder node
        if (!nodo.estado && !nodo.proveedor && !nodo.monto) {
          source = $("#nodo-contrato-mas").html();
        } else {
          source = $("#nodo-contrato").html();
        }
        context = { titulo: nodo.name, fecha: nodo.fechaFirmaContrato, estado: nodo.estado, proveedor: _.trunc(nodo.proveedor, 30), monto: nodo.monto };
        break;

      case 4:
        source = $("#nodo-ampliacion").html();
        context = { titulo: nodo.name, fecha: nodo.fecha, estado: nodo.estado, monto: nodo.monto };
        break;
    }

    if (context.estado) {
      context.rojo = _.contains(['Cancelado', 'Anulado', 'Desierto'], context.estado);
      context.naranja = _.contains(['Suspendido', 'Impugnado'], context.estado);
      context.verde = _.contains(['Adjudicado', 'Ejecutado', 'Publicado'], context.estado) || !(context.rojo || context.naranja);
    }
    template = Handlebars.compile(source);
    content = template(context);

    return content;
  }

  function widthByNode(nodo) {
    var width = 142;
    //Contrato
    if (nodo.depth === 3) {
      width = 274;
    }
    return width;
  }

  function yOffsetByNode(nodo) {
    var yOffset = -71;
    //Contrato
    if (nodo.depth === 3) {
      yOffset = -50;
    }
    return yOffset;
  }

  function buildTree(niter) {
    // Compute the new tree layout.
    niter = niter = niter || 0;
    var treeHeight = height + niter * 50;
    tree = d3.layout.tree().size([treeHeight, width]);
    var nodes = tree.nodes(root).reverse(),
      links = tree.links(nodes);
    //console.log(treeHeight);
    var minNodeSpacing = _.reduce(nodes, function (min, node, i, nodes) {
      var spacing;
      if (i + 1 < nodes.length && node.depth === nodes[i + 1].depth) {
        spacing = Math.abs(node.x - nodes[i + 1].x);
        min = (spacing < min) ? spacing : min;
      }
      return min;
    }, height);

    if (minNodeSpacing < 145) {
      //Iterate until minimum required height is found
      return buildTree(niter + 1);
    } else {
      return {
        nodes: nodes,
        links: links,
        height: treeHeight
      };
    }
  }

  function trimMargins(nodes, currentHeight) {
    var margin = 75;
    var maxNodesByDepth = _(nodes).countBy(function (node) { return node.depth; }).values().max();
    var minX = _(nodes).pluck('x').min();
    if (maxNodesByDepth > 1) {
      _.each(nodes, function (node) {
        node.x = node.x - (minX - margin);
      });
    }
    var maxX = _(nodes).pluck('x').max();
    return (maxNodesByDepth > 1) ? maxX + margin : currentHeight;
  }

  function update(source, singleNode) {
    var treeElems = buildTree();
    var nodes = treeElems.nodes;
    var links = treeElems.links;

    // Compute the new tree layout.
    var nodes = tree.nodes(root).reverse(),
      links = tree.links(nodes);
    var trimmedHeight = trimMargins(nodes, treeElems.height);

    // Normalize for fixed-depth.
    nodes.forEach(function (d) {
      var offset = (d.depth === 4) ? 125 : 0;
      d.y = d.depth * 180 + offset;
    });

    if (singleNode) {
      nodes[0].y = width / 2;
    }
    // Update the nodes…
    var node = svg.selectAll("g.node")
      .data(nodes, function (d) { return d.id || (d.id = ++i); });

    var maxDepth = _(nodes).pluck('depth').max();

    // Enter any new nodes at the parent's previous position.
    var nodeEnter = node.enter().append("g")
      .attr("class", "node")
      .attr("transform", function (d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
      .style("cursor", function (nodo) { return nodo.depth === maxDepth && nodo.depth !== 1 ? "default" : "pointer"; })
      .on("click", clickChoice);

    nodeEnter.append('foreignObject')
      .attr("x", -71)
      .attr("y", yOffsetByNode)
      .attr("width", widthByNode)
      .attr("height", 142)
      .append("xhtml:div")
      .html(contentByNode);

    // Transition nodes to their new position.
    var nodeUpdate = node.transition()
      .duration(duration)
      .attr("transform", function (d) { return "translate(" + d.y + "," + d.x + ")"; });

    // Transition exiting nodes to the parent's new position.
    var nodeExit = node.exit().transition()
      .duration(duration)
      .attr("transform", function (d) { return "translate(" + source.y + "," + source.x + ")"; })
      .remove();

    nodeExit.select("circle")
      .attr("r", 1e-6);

    nodeExit.select("text")
      .style("fill-opacity", 1e-6);

    // Update the links…
    var link = svg.selectAll("path.link")
      .data(links, function (d) { return d.target.id; });

    // Enter any new links at the parent's previous position.
    link.enter().insert("path", "g")
      .attr("class", "link")
      .attr("fill", "white")
      .attr("stroke-width", "10px")
      .attr("d", function (d) {
        var o = { x: source.x0, y: source.y0 };
        return diagonal({ source: o, target: o });
      });

    // Transition links to their new position.
    link.transition()
      .duration(duration)
      .attr("d", diagonal);

    // Transition exiting nodes to the parent's new position.
    link.exit().transition()
      .duration(duration)
      .attr("d", function (d) {
        var o = { x: source.x, y: source.y };
        return diagonal({ source: o, target: o });
      })
      .each("end", _.once(function () {
        d3.select('svg')
          .attr("height", trimmedHeight + margin.top + margin.bottom);
      }))
      .remove();

    // Stash the old positions for transition.
    nodes.forEach(function (d) {
      d.x0 = d.x;
      d.y0 = d.y;
    });

    return trimmedHeight;
  }

  var DELAY = 700,
    clicks = 0,
    timer = null;

  function clickChoice(d) {
    clicks++;  //count clicks
    if (clicks === 1) {
      timer = setTimeout(function () {
        click(d);
        clicks = 0;  //after action performed, reset counter
      }, DELAY);
    } else {
      clearTimeout(timer);  //prevent single-click action
      dblclick(d);
      clicks = 0;  //after action performed, reset counter
    }
  }

  function click(d) {
    //console.log(d);
    if (d.name == 'Convocatoria') {
      window.open(getLlamadoUrl(d.nodeId));
    }
  }

  function getLlamadoUrl(id) {
    var baseUrl = 'https://www.contrataciones.gov.py/licitaciones/convocatoria/';
    var llamadoParam = encodeURIComponent(id) + '.html';
    return baseUrl + llamadoParam;
  }

  // Toggle children on dblclick.
  function dblclick(d) {
    var updateFlag;
    if (d.children) {
      updateFlag = (d.children && d.children.length > 0);
      d._children = d.children;
      d.children = null;
    } else {
      updateFlag = (d._children && d._children.length > 0);
      d.children = d._children;
      d._children = null;
    }

    //Avoid updating if node has no children
    if (updateFlag) {
      update(d);
    }
  }

  return { showTree: showTree };

})();

CGraph.util = (function () {
  var getUrlParameter = function (sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
      var sParameterName = sURLVariables[i].split('=');
      if (sParameterName[0] == sParam) {
        return sParameterName[1];
      }
    }
  }
  return {
    getUrlParameter: getUrlParameter
  }
})();


$(document).ready(function () {
  //Remote
  var defaultId = CGraph.util.getUrlParameter('id_llamado') || '365292';
  $('input').val(defaultId);
  CGraph.treeBuilder.showTree(defaultId);
  $('input').keypress(function (e) {
    if (e.which == 13) {
      CGraph.treeBuilder.showTree($('input').val());
      return false;
    }
  });

  /* Cuando el buscador este actualizado descomentar la llamada al servicio y sacar el mensaje de error */
  $('#draw-trigger').on('click', function () {
    CGraph.treeBuilder.showTree($('input').val());
    //toastr.error('Esta visualización se encuentra en mantenimiento. Disculpe los inconvenientes.');
    return false;
  });

  $('[href="#embedder"]').on('click', function () {
    var idLlamado = $('#id-llamado').val();
    var code = '<iframe src="https://www.contrataciones.gov.py/datos/visualizaciones/etapas-licitacion-emb?id_llamado='
      + idLlamado +
      '" style=&quot;width: 1200px; height: 1200px; border: none; padding: 0; margin: 0;"></iframe>'
    $('#embedcode').val(code);
  });


  /*d3.json('scripts/llamado.json', function(llamadoTree){
    drawTree(llamadoTree);
  });*/
});